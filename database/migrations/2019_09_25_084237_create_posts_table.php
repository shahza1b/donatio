<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('title', 50);
            $table->string('category', 20);
            $table->string('country');
            $table->string('city');
            $table->string('area', 100)->nullable();
            $table->string('product_condition');
            $table->string('approx_value');
            $table->string('post_code')->nullable();
            $table->string('tags')->nullable();
            $table->string('hearts')->default(0);
            $table->string('shares')->default(0);
            $table->string('views')->default(0);
            $table->string('description', 10000);
            $table->string('images', 2000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
