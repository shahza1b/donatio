<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    public function States()
    {
        return $this->hasMany('App\State');
    }
    
    
    public function Cities()
    {
        return $this->hasMany('App\City');
    }
}
