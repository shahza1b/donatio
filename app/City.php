<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    public function State()
    {
        return $this->belongsTo('App\City');
    }
}
