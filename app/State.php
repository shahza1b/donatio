<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    public function Cities()
    {
        return $this->hasMany('App\City');
    }

    public function Country()
    {
        return $this->belongsTo('App\Country');
    }
}

