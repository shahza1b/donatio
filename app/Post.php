<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', "user_id");
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "title",
        "category",
        "country",
        "city",
        "area",
        "product_condition",
        "approx_value",
        "post_code",
        "tags",
        "hearts",
        "shares",
        "views",
        "description",
        "images",
        "url"
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
