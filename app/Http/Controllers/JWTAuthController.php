<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;
use Storage;

class UserController extends Controller
{
    // DECODE AND GET PAYLOAD FROM TOKEN
    private function getPayload($payload){
        $token = $payload->bearerToken();
        $decodedToken = JWTAuth::setToken($token)->getPayload();
        return $decodedToken;
    }


    // GENERATE AUTHENTICATION TOKEN
    private function getToken($email, $password)
    {
        $token = null;
        $credentials = [
            'email' => $email,
            'password' => $password,
        ];
        $user = User::where('email', $email)->first();

        $customClaims = [
            'email' => $email,
            'name' => $user->name,
            'username' => $user->username,
            'organization' => 'donatio',
        ];
        try {
            if (!$token = JWTAuth::claims($customClaims)->attempt($credentials)) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'Password or email is invalid',
                    'token' => $token,
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'Token creation failed',
            ]);
        }
        return $token;
    }

    // Login
    public function login(Request $request)
    {
        // return ("login");
        $email = strtolower($request->email);
        $pass = $request->password;
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = \App\User::where("email", $email)->get()->first();
            if (empty($user)) {
                return response()->json(['success' => false, 'general' => "Invalid Credentials"], 400);
                exit(1);
            } else {
                $token = self::getToken($email, $pass);
            }
        } else {
            $user = \App\User::where("username", $email)->get()->first();
            if (empty($user)) {
                return response()->json(['success' => false, 'general' => "Invalid Credentials"], 400);
                exit(1);
            } else {

                $email_now = $user->email;
                $token = self::getToken($email_now, $pass);
            }
        }
        if ($user && \Hash::check($pass, $user->password)) // The passwords match...
        {
            $user->auth_token = $token;
            $user->save();
            $response = ['success' => true, 'token' => $token, 'username'=>$user->username, 'profileImage' => $user->profileImage];
        } else {
            return response()->json(['success' => false, 'general' => "Invalid Credentials"], 400);
        }

        return response()->json($response, 201);
    }

    // Register
    public function register(Request $request)
    {
        $userName = strtolower($request->username);
        $email = strtolower($request->email);

        $user = \App\User::where('username', $userName)->get()->first();
        if ($user) {
            $response = ['message' => 'Username already exists'];
            return response()->json($response, 400);
            die();
        } else {
            $user = \App\User::where('email', $email)->get()->first();
            if ($user) {
                $response = ['message' => 'An account with this email already exists'];
                return response()->json($response, 400);
                die();
            }
            $payload = [
                'password' => \Hash::make($request->password),
                'email' => $email,
                'name' => $request->name,
                'username' => $userName,
                'profileImage' => '/storage/profileImages/user.png',
                'auth_token' => '',
            ];
            $user = new \App\User($payload);
            if ($user->save()) {
                $token = self::getToken($email, $request->password); // generate user token
                if (!is_string($token)) {
                    return response()->json(['success' => false, 'data' => 'Token generation failed'], 500);
                } else {
                    $user->auth_token = $token;
                    $user->save();
                }

                $response = ['message' => "User Registered Successfully"];
            } else {
                $response = ['message' => 'Couldnt register user'];
                return response()->json($response, 500);
            }
        }
        return response()->json($response, 201);
    }

    
    // GET AUTHENTICATED USER DATA
    public function getAuthenticatedUser(Request $request)
    {
        $decodedToken = self::getPayload($request);
        $data = User::where('email', $decodedToken['email'])->get([
            'id',
            'name',
            'about',
            'location',
            'contact',
            'email',
            'username',
            'telephone',
            'gender',
            'profileImage',
            'area'
        ]);
        return response()->json($data);
    }

    public function getUserRating()
    {
        $userRating = User::find(8)->Rating;
        return $userRating;
    }

    // Update Profile
    public function updateProfile(Request $request)
    {
        
        try {
            $decodedToken = self::getPayload($request);
            $user = User::where('email', $decodedToken['email'])->first();
            $user->name = $request->name;
            $user->about = $request->about;
            $user->location = $request->location;
            $user->contact = $request->contact;
            $user->telephone = $request->telephone;
            $user->gender = $request->gender;
            $user->area = $request->area;
            if($user->save()){
                if ($request->profileImage !== $user->profileImage) {
                    // Storage::delete($user->profileImage);
                    $postImage = "profileImage/profile_image" . str_random(20) . "_" .  date("Y-m-d") . '.' . 'png';
                    $base64_image = $request->profileImage;
                    @list($type, $file_data) = explode(';', $base64_image);
                    @list(, $file_data) = explode(',', $file_data);
                    Storage::disk('public')->put($postImage, base64_decode($file_data));

                    $user->profileImage = Storage::url($postImage);
                    // $user->profileImage = $_SERVER['DOCUMENT_ROOT']."/storage/".$postImage;
                    $user->save();
                }
                $response = ['profile' => 'Profile Updated Successfully'];
                return response()->json($response, 201);
            } else {
                $response = ['message' => 'Something went wrong...'];
                return response()->json($response, 500);   
            }
            
        } catch(Exception $e) {
            $response = ['message' => 'Something went wrong...'];
            return response()->json($response, 500);
        }

        
    }

    // Change password
    public function changePassword(Request $request)
    {
        $token = $request->bearerToken();
        $decodedToken = JWTAuth::setToken($token)->getPayload();

        $user = User::where('email', $decodedToken['email'])->get()->first();

        $oldPass = $request->oldPassword;
        $newPass = $request->newPassword;

        if ($user && \Hash::check($oldPass, $user->password)) {
            $user->password = \Hash::make($newPass);
            $user->save();

            return response()->json(['password' => 'Password Updated Successfully'], 201);
        } else {
            return response()->json(['password' => 'Old Password is incorrect'], 403);
        }

    }

}
