<?php

namespace App\Http\Controllers;

use App\Country;
use App\State;
use App\City;

class CountriesController extends Controller
{
    //
    public function get($search)
    {
        $countriesList = Country::where('name', 'LIKE', '%'.$search.'%' )->limit(10)->get();
        return response()->json(['countriesList' => $countriesList]);
    }

    public function states($search)
    {
        $states = State::where('name', 'LIKE', '%'.$search.'%' )->limit(10)->get();
        return response()->json(['states' => $states]);
    }

    public function cities($search)
    {
        $cities = City::where('name', 'LIKE', '%'.$search.'%' )->limit(10)->get();
        return response()->json(['cities' => $cities]);
    }

}
