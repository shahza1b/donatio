<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Post;
use Storage;

class DonorController extends Controller
{
    //
    public function get(){
        $donors = User::orderBy('id', 'desc')
                    // ->limit(12)
                    ->get(['name', 'location', 'profileImage', 'username']);
        return response()->json(['donors' => $donors ]);
    }

    public function count(){
        $count = Post::count(DB::raw('DISTINCT user_id'));
        return response()->json($count);
    }

    public function show($username){
        $data = User::where('username', $username)->first();
        $data->donations = Post::where('user_id', $data->id)->where('donated', 0)->get();
        $data->donated = Post::where('user_id', $data->id)->where('donated', 1)->get();

        foreach($data->donations as $donation){
            $donation->images = Storage::url(explode(';', $donation->images)[0]);
        }
        
        foreach($data->donated as $donated){
            $donated->images = Storage::url(explode(';', $donated->images)[0]);
        }

        if($data){
            return response()->json($data);
        } else {
            return response()->json("Not Found", 404);
        }
    }

}


