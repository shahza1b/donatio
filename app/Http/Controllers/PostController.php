<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Storage;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{

    // DECODE AND GET PAYLOAD FROM TOKEN
    private function getPayload($payload)
    {
        $token = $payload->bearerToken();
        $decodedToken = JWTAuth::setToken($token)->getPayload();
        return $decodedToken;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display total count of donations/posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function count()
    {
        $count = Post::count();
        return response()->json($count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $decodedToken = self::getPayload($request);
        $user = User::where('email', $decodedToken['email'])->first();
        //
        $payload = [
            "user_id" => $user->id,
            "title" => $request->title,
            "category" => $request->category,
            "country" => $request->country,
            "city" => $request->city,
            "area" => $request->area,
            "product_condition" => $request->product_condition,
            "approx_value" => $request->approx_value,
            "post_code" => $request->post_code,
            "tags" => $request->tags,
            "description" => $request->description,
            "images" => "",
            'url' => $request->url,
        ];

        $post = new Post($payload);
        if ($post->save()) {
            // $post = Post::where('title', $request->title)->get()->first();
            foreach ($request->images as $file) {
                $name = $post->id . "_" . str_random(10) . "_" . date("Y-m-d") . '.' . 'png';
                $base64_image = $file; // your base64 encoded
                @list($type, $file_data) = explode(';', $base64_image);
                @list(, $file_data) = explode(',', $file_data);
                Storage::disk('public')->put($name, base64_decode($file_data));
                $image_names[] = $name;
            }
            $image = implode(";", $image_names);
            $post->images = $image;
            $post->save();
            // $data = Post::where('user_id', $post->user_id)->get();

            $response = ['success' => true, 'data' => "Post created successfully"];
        } else {
            $response = ['success' => false, 'data' => "Couldn't create post"];
        }

        return response()->json($response, 201);
    }

    /**
     * Show a single post/donation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($username, $title)
    {
        $user = User::select('id')->where('username', $username)->first();

        $url = $username . "/" . $title;
        $post = Post::with(array('user' => function ($query) {
            $query->select('id', 'name', 'location', 'profileImage');
        }))->where('url', $url)->first();

        if ($post) {
            $images = explode(';', $post->images);
            $newImages = [];
            foreach ($images as $image) {
                array_push($newImages, (object) [
                    'original' => '/storage/' . $image,
                    'thumbnail' => '/storage/' . $image,
                ]);
            }
            $post->images = $newImages;


            // More from this user
            $more = Post::with(array('user' => function ($query) {
                $query->select('id', 'name', 'location', 'profileImage', 'username');
            }))->where('user_id', $user->id)->get();

            foreach ($more as $P){
                $Images = explode(';', $P->images);
                $new_Images = [];
                foreach ($Images as $Image) {
                    array_push($new_Images, (object) [
                        'original' => '/storage/' . $Image,
                        'thumbnail' => '/storage/' . $Image,
                    ]);
                }
                $P->images = $new_Images;
            }

            // Related
            $related = Post::with(array('user' => function ($query) {
                $query->select('id', 'name', 'location', 'profileImage', 'username');
            }))->where('category', $post->category)->get();

            foreach ($related as $P){
                $Images = explode(';', $P->images);
                $new_Images = [];
                foreach ($Images as $Image) {
                    array_push($new_Images, (object) [
                        'original' => '/storage/' . $Image,
                        'thumbnail' => '/storage/' . $Image,
                    ]);
                }
                $P->images = $new_Images;
            }

            return response()->json(['post' => $post, 'more' => $more, 'related' => $related]);
        } else {
            return response()->json("Post Not Found", 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function showAllUserPosts(Request $request)
    {
        //
        // return $request->user_id;
        $data = Post::where('user_id', $request->user_id)->get();
        if (!empty($data)) {
            $response = ['success' => true, 'data' => $data];
        } else {
            $response = ['success' => false, 'data' => "No posts exists"];
        }

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function showLatestPosts()
    {
        //
        // $posts = Post::orderBy('id', 'desc')->get();
        // foreach ($posts as $post) {
        //     $author_name[] = User::select('name', 'username', 'profileImage')->where('id', $post->user_id)->first();
        // }
        // $response = ["data" => $posts, "authors" => $author_name];

        $posts = Post::with(array('user' => function ($query) {
            $query->select('id', 'name', 'location', 'profileImage', 'username');
        }))->orderBy('id', 'desc')->get();

        foreach ($posts as $post){
            $post->images = Storage::url(explode(';', $post->images)[0]);
        }
        return response()->json($posts, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
