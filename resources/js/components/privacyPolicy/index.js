import React from "react";
import Header from "../reusableComponents/Header";
import Footer from "../reusableComponents/Footer";
import PrivacyPolicyBody from "./PrivacyPolicyBody";

const PrivacyPolicy = () => {
    return (
        <div>
            {/* <Header /> */}
            <PrivacyPolicyBody />
            {/* <Footer /> */}
        </div>
    );
};

export default PrivacyPolicy;
