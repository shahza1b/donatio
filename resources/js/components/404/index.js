import React from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

const scrollToTop = () => {
    window.scroll({
        top: 0,
        left: 0,
        behavior: "smooth"
    });
};

export default function _404({title}) {
    return (
        <div id="notfound">
            <div className="notfound">
                <div className="notfound-404">
                    <h1>404</h1>
                    <h2>{title} not found</h2>
                </div>
                <Link to="/" onClick={scrollToTop}>
                    Homepage
                </Link>
            </div>
        </div>
    );
}

_404.prototype = {
    title: PropTypes.string.isRequired
}