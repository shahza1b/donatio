import React from "react";
import Header from "../reusableComponents/Header";
import Footer from "../reusableComponents/Footer";
import AboutContent from "./AboutContent";

const About = () => {
    return (
        <div>
            {/* <Header /> */}
            <AboutContent />
            {/* <Footer /> */}
        </div>
    );
};

export default About;
