import React from "react";
import { Link } from "react-router-dom";

const AboutContent = () => {
    return (
        <section className="irs-about-field">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="irs-about-col">
                            <h2>WHO WE ARE</h2>
                            <p>
                                Donatio is a donation-based platform that is
                                changing the way of giving your needy fellows.
                                The Donatio board is more focused on
                                facilitating a donor by giving them an online
                                space where they can post the items they are
                                looking to donate. You can easily contribute by
                                giving your used or unused accessories by
                                sitting at home, yet just to put an ethical
                                stance they must be in a reasonable condition
                                which can be utilized by the donee. Just follow
                                the few steps and your product is there online
                                to fulfil someone’s need. So if you have any
                                useful thing at your home which you are not
                                using then visit Donatio and be generous in
                                making it useful for someone who is in need of
                                it!
                            </p>
                            <p className="teresa">
                                {" "}
                                “It’s not how much we give but how much love we
                                put into giving”---mother Teresa
                            </p>
                            <h4>About Green Systems:</h4>
                            <p>
                                Green Systems is a software solutions which
                                takes responsibility of producing quality
                                solutions with an element of adding value to the
                                society. It not only focuses on its product but
                                also keeps an eye over things that can produce
                                innovative solutions covering the ethical and
                                moral grounds of a society.
                            </p>
                            <div className="about_btn">
                                <Link
                                    to="#"
                                    className="btn btn-default irs-big-btn"
                                    role="button"
                                    style={{
                                        borderRadius: "25px",
                                        padding: "0.7em"
                                    }}>
                                    JOIN US NOW
                                </Link>
                            </div>
                        </div>
                        <div className="about_img">
                            <img
                                src="../images/about_us_img.png"
                                alt="About Us"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default AboutContent;
