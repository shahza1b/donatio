import React from "react";
import Modal from "react-bootstrap/Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { withFormik } from "formik";
// import Slide from "react-reveal/Slide";


const ForgotPassword = props => {
    const [show, setShow] = React.useState(false);
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit
    } = props;
    return (
        <div>
            {/* <a
                style={{ color: "#ef004c !important" }}
                onClick={() => setShow(true)}
                to="#"
                className="txt3"
            >
                Forgot Password
            </a>
            <Modal
                show={show}
                onHide={() => setShow(false)}
                backdrop={false}
                dialogClassName="modal-10w"
                aria-labelledby="example-custom-modal-styling-title"
            >
                <Modal.Header closeButton>
                    <button
                        onClick={() => setShow(false)}
                        className="p-t-5 p-r-15"
                    >
                        <FontAwesomeIcon icon={faChevronLeft} />
                    </button>
                    <Modal.Title id="forgot-password-modal-title">
                        Recover your password
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body> */}
                    <form
                        id="loginForm"
                        className="login100-form validate-form p-l-15 p-r-15"
                        onSubmit={handleSubmit}
                    >
                        <div className="row row-centered">
                            <div
                                className="col"
                                style={{ paddingBottom: "1em" }}
                            >
                                <br />
                                Please enter your email associated with your
                                account
                            </div>
                        </div>
                        <div className="row row-centered">
                            <div
                                className="col"
                                style={{ paddingBottom: "1em" }}
                            >
                                We'll send a link to reset your password
                            </div>
                        </div>
                        <div
                            className="wrap-input100 validate-input m-b-16"
                            data-validate="Please enter username"
                        >
                            <input
                                required
                                className="input100"
                                type="email"
                                name="email"
                                placeholder="Email Address"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                            />
                            <span className="focus-input100" />
                        </div>
                        <div className="container-login100-form-btn">
                            <button
                                className="login100-form-btn"
                                type="submit"
                                style={{ width: "100%" }}
                            >
                                SEND EMAIL
                            </button>
                        </div>
                    </form>
                {/* </Modal.Body>
            </Modal> */}
        </div>
    );
};
export default withFormik({
    mapPropsToValues: () => ({
        email: ""
    }),

    handleSubmit: (values, { setSubmitting }) => {
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: "BasicForm"
})(ForgotPassword);
