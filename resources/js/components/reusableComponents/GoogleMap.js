import React from "react";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";

class MapContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // stores: [{ latitude: 32.178845, longitude: 74.182927 }]
            stores: [{ latitude: 32.178983, longitude: 74.182551 }]
        };
    }

    displayMarkers = () => {
        return this.state.stores.map((store, index) => {
            return (
                <Marker
                    key={index}
                    id={index}
                    position={{
                        lat: store.latitude,
                        lng: store.longitude
                    }}
                />
            );
        });
    };

    render() {
        return (
            <div className="page">
                <section>
                    <Map
                        google={this.props.google}
                        zoom={14}
                        style={mapStyles}
                        initialCenter={{ lat: 32.17981, lng: 74.183109 }}>
                        {this.displayMarkers()}
                    </Map>
                </section>
            </div>
        );
    }
}

const mapStyles = {
    width: "100%",
    height: "100%"
};
export default GoogleApiWrapper({
    apiKey: "AIzaSyCO71pyuHAiQ1zLahGoIJXOA5lHUJDX-6U"
})(MapContainer);
