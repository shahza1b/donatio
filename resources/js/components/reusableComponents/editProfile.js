import React from "react";
import { withFormik } from "formik";
import { connect } from "react-redux";
import Spinner from "react-spinner-material";
import { updateProfile } from "../Store/Actions";
import { SET_LOADING, UNSET_LOADING } from "../Store/consts";
import FileBase64 from "react-file-base64";

class EditProfile extends React.Component {
    state = {
        profileImage: ""
    };

    uploadTrigger = () => {
        document.querySelector('.uploadImage input').click()
    }
    getFiles = (file) => {
        this.props.setFieldValue('profileImage', file.base64)
    };

    render() {
        const {
            values,
            // touched,
            // errors,
            handleBlur,
            handleSubmit,
            handleChange,
            loading
        } = this.props;
        return (
            <div>
                <div>
                    <div
                        style={{
                            height: "130px",
                            backgroundColor: "#ef004c",
                            position: "absolute",
                            width: "100%",
                            zIndex: "1"
                        }}
                    />
                    {/* <a href="#"> */}
                        <div style={{ position: "relative" }}>
                            <img
                                src={values.profileImage}
                                className="rounded-circle mx-auto d-block"
                                alt="Profile Image"
                                style={{
                                    width: "160px",
                                    height: "160px",
                                    position: "relative",
                                    top: "39px",
                                    zIndex: "999",
                                    border: "4px solid white",
                                    borderRadius: "50px",
                                    marginBottom: "100px",
                                    boxShadow: "0px 17px #00000014",
                                    background: "white"
                                }}
                            />
                        </div>
                        <i
                            className="fas fa-camera"
                            style={{
                                cursor: "pointer",
                                position: "absolute",
                                top: "146px",
                                left: "726px",
                                zIndex: "999",
                                border: "2px solid #ef004c",
                                color: "#ef004c",
                                fontSize: "19px",
                                borderRadius: "50%",
                                background: "white",
                                padding: "8px"
                            }}
                            onClick={this.uploadTrigger}
                        ></i>
                        {/* <input type="file" hidden="hidden" id="uploadImage" onChange={this.getFiles}/> */}
                        <div className="d-none uploadImage">
                                    <FileBase64
                                        multiple={false}
                                        onDone={this.getFiles}
                                    />
                                </div>
                    {/* </a> */}
                </div>
                <div className="row row-centered mt-5 mb-5">
                    <div className="col-md-12">
                        <p className="social_icons">
                            <a href="#" style={{ padding: "0px 6px" }}>
                                <i
                                    style={{
                                        color: "#4267b2",
                                        fontSize: "30px"
                                    }}
                                    className="fa fa-facebook-square"
                                    aria-hidden="true"
                                />
                            </a>
                            <a style={{ padding: "0px 6px" }} href="#">
                                <i
                                    style={{ fontSize: "30px" }}
                                    className="fa fa-twitter-square"
                                    aria-hidden="true"
                                />
                            </a>
                            <a style={{ padding: "0px 6px" }} href="#">
                                <i
                                    style={{ fontSize: "30px" }}
                                    className="fa fa-pinterest-square"
                                    aria-hidden="true"
                                />
                            </a>
                            <a style={{ padding: "0px 6px" }} href="#">
                                <i
                                    style={{ fontSize: "30px" }}
                                    className="fas fa-envelope-square"
                                    aria-hidden="true"
                                ></i>
                            </a>
                            <a style={{ padding: "0px 6px" }} href="#">
                                <i
                                    style={{ fontSize: "30px" }}
                                    className="fa fa-phone-square"
                                ></i>
                            </a>
                            <a style={{ padding: "0px 6px" }} href="#">
                                <i
                                    style={{ fontSize: "30px" }}
                                    className="fa fa-address-card"
                                ></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div className="container">
                    <div className="row ">
                        <form
                            id="loginForm"
                            className="login100-form validate-form p-l-15 p-r-15"
                            onSubmit={handleSubmit}
                        >
                            {/* <span className="login100-form-title">Sign In</span> */}
                            <div
                                className="row"
                                style={{
                                    justifyContent: "center",
                                    padding: "0px 120px"
                                }}
                            >
                                <div className="col-md-4">
                                    <div className="m-b-16 ">
                                        <div className="wrap-input100 m-b-16 validate-input">
                                            <input
                                                autoFocus
                                                required
                                                className="input100"
                                                type="text"
                                                name="name"
                                                placeholder="Name"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.name}
                                            />
                                        </div>
                                        <div className="wrap-input100 m-b-16 validate-input">
                                            <input
                                                className="input100"
                                                type="string"
                                                name="telephone"
                                                placeholder="Telephone(Optional)"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.telephone}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            className="input100 disabled"
                                            value={values.email}
                                            disabled
                                            style={{
                                                cursor: "no-drop",
                                                background:
                                                    "#80808012 !important"
                                            }}
                                        />
                                    </div>
                                    <select
                                        className="input100 slect-field form-control"
                                        name="gender"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.gender}
                                    >
                                        <option defaultValue value="n_a">
                                            Gender
                                        </option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                                <div className="col-md-4">
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            required
                                            className="input100"
                                            type="string"
                                            name="contact"
                                            placeholder="Contact"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.contact}
                                        />
                                    </div>
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            required
                                            className="input100"
                                            type="text"
                                            name="area"
                                            placeholder="Area"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.area}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div
                                className="row"
                                style={{
                                    justifyContent: "center",
                                    padding: "0px 120px"
                                }}
                            >
                                <div className="col-md-4">
                                    {" "}
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            className="input100"
                                            type="text"
                                            name="location"
                                            placeholder="Location"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.location}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-8">
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            className="input100"
                                            type="text"
                                            name="about"
                                            placeholder="About"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.about}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div
                                className="row mb-5"
                                style={{
                                    justifyContent: "center",
                                    padding: "0px 120px"
                                }}
                            >
                                <div className="col-md-6">
                                    <label className="CheckboxContainer">
                                        <span
                                            style={{
                                                fontSize: "16px",
                                                color: "#0000009e"
                                            }}
                                        >
                                            I want to stay anonymous
                                        </span>
                                        <input type="checkbox" />
                                        <span
                                            className="checkmark"
                                            style={{ marginTop: "7px" }}
                                        ></span>
                                    </label>
                                </div>
                                <div className="col-md-6">
                                    <button
                                        type="submit"
                                        style={{
                                            background: "#41b541",
                                            borderRadius: "25px",
                                            padding: "15px 105px",
                                            color: "white",
                                            float: "right"
                                        }}
                                        disabled={loading}
                                    >
                                        {loading ? (
                                            <Spinner
                                                size={23}
                                                spinnerColor={"#fff"}
                                                spinnerWidth={2}
                                                visible={true}
                                            />
                                        ) : (
                                            "UPDATE"
                                        )}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.user.isLoggedIn,
        user: state.user.credentials,
        loading: state.UI.loading
    };
}

export default connect(mapStateToProps)(
    withFormik({
        mapPropsToValues: props => {
            return { ...props.user };
        },

        handleSubmit: (values, { props, setSubmitting }) => {
            props.dispatch({ type: SET_LOADING });
            setTimeout(async () => {
                console.log(values)
                await props.dispatch(updateProfile(values));
                props.dispatch({ type: UNSET_LOADING });
                setSubmitting(false);
            }, 1000);
        }
    })(EditProfile)
);
