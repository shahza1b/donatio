import React from "react";
import Modal from "react-bootstrap/Modal";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const ReportPostModal = () => {
    const [show, setShow] = React.useState(false);
    return (
        <div>
            <a
                onClick={() => setShow(true)}
                href="#"
                className="txt3 "
                style={{
                    color: "#ef004c !important",
                    textTransform: "capitalize"
                }}>
                Report Post
            </a>
            <Modal
                show={show}
                onHide={() => setShow(false)}
                // backdrop={false}
                // style={{ height: "40vh" }}
                dialogClassName="modal-10w"
                aria-labelledby="example-custom-modal-styling-title">
                <Modal.Header closeButton>
                    {/* <button
                        onClick={() => setShow(false)}
                        className="p-t-5 p-r-15">
                        <FontAwesomeIcon icon={faChevronLeft} />
                    </button> */}
                    <Modal.Title id="forgot-password-modal-title">
                        REPORT POST
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form
                        id="loginForm"
                        className="login100-form validate-form p-l-15 p-r-15">
                        <div style={{ display: "inline-flex" }}>
                            <input
                                type="radio"
                                name="report"
                                value="fake-post"
                            />
                            <span style={{ padding: "0.6em 1em" }}>
                                Post is fake
                            </span>
                        </div>
                        <br />
                        <div style={{ display: "inline-flex" }}>
                            <input
                                type="radio"
                                name="report"
                                value="incorrect-address"
                            />
                            <span style={{ padding: "0.6em 1em" }}>
                                Address is not correct
                            </span>
                        </div>

                        <br />
                        <div style={{ display: "inline-flex" }}>
                            <input
                                type="radio"
                                name="report"
                                value="unauthorized_sale"
                            />
                            <span style={{ padding: "0.6em 1em" }}>
                                Unauthorized Item
                            </span>
                        </div>
                        <br />
                        <div style={{ display: "inline-flex" }}>
                            <input
                                type="radio"
                                name="report"
                                value="something_else"
                            />
                            <span style={{ padding: "0.6em 1em" }}>
                                Something Else
                            </span>
                        </div>
                        <textarea
                            className="input1000"
                            name="userReport"
                            id="something_else_text"
                            placeholder="Write your comments here..."
                            style={{
                                width: "100%",
                                padding: "0.3em 1em",
                                lineHeight: "unset",
                                borderRadius: "10px",
                                marginTop: "0px",
                                marginBottom: "0px",
                                height: "107px"
                            }}
                            rows="3"></textarea>
                        <div className="container-login100-form-btn pt-3 pb-3">
                            <button
                                className="login100-form-btn"
                                type="button"
                                style={{ width: "100%" }}>
                                REPORT
                            </button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        </div>
    );
};

export default ReportPostModal;
