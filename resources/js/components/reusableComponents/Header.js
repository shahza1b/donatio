import React, { useEffect }from "react";
import { Link, withRouter } from "react-router-dom";
import LoginModal from "./loginModal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import { userLogout } from "../Store/Actions";
import { faBell } from "@fortawesome/free-regular-svg-icons";
import ResetPassword from "./ResetPassword";
import swal from 'sweetalert';


const Header = props => {
    const { isLoggedIn, user, history } = props
    useEffect(() => {
        // console.log(props)
    }, [])
    const handleClick = () => {
        swal("Do you really want to logout?", {
            buttons: {
              cancel: false,
              Yes: true,
            },
          })
          .then((value) => {
            if(value) {
                history.push("/")
                setTimeout(() => props.userLogout(), 10)
            }
          });
        
    };
    return (
        <header className="irs-main-header scrollingto-fixed">
            <div className="irs-header-top-bar " id="header">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-2 col-sm-2 col-xs-12 main_logo">
                            <div className="irs-main-logo">
                                <Link to="/">
                                    <img src="https://donatio.love/resources/assets/public/images/logo2.png" />
                                </Link>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-5 col-xs-12 main_menus">
                            <div className="irs-social text-left irs-center-2 top-menu">
                                <div className="col-lg-8 col-md-10 col-sm-12">
                                    <div className="nav_icon">
                                        <img src="https://donatio.love/resources/assets/public/images/nav_icon.png" />
                                    </div>
                                    <nav>
                                        <ul className="navbar-left">
                                            <li>
                                                <Link
                                                    className={
                                                        window.location
                                                            .pathname ===
                                                        "/alldonations"
                                                            ? "focused-tab" //if condition true
                                                            : "null" // if false
                                                    }
                                                    to="/alldonations"
                                                >
                                                    Donations
                                                </Link>
                                            </li>
                                            <li>
                                                <Link
                                                    className={
                                                        window.location
                                                            .pathname ===
                                                        "/donors"
                                                            ? "focused-tab"
                                                            : "null"
                                                    }
                                                    to="/donors"
                                                >
                                                    Donors
                                                </Link>{" "}
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-5 col-xs-12 col-md-offset-0 login_links">
                            <div
                                className={
                                    isLoggedIn
                                        ? "irs-header-top-col text-right irs-center-2 top-menu login_reg_links mt-1"
                                        : "irs-header-top-col text-right irs-center-2 top-menu login_reg_links"
                                }
                            >
                                {/* Add mt-1 class only when the user is logged in */}
                                <ul>
                                    <li
                                        style={{
                                            padding: "0px",
                                            margin: 0
                                        }}
                                    >
                                        <span
                                            className={
                                                isLoggedIn === true
                                                    ? "irs-header-top-col text-right irs-center-2 top-menu login_reg_links mt-1"
                                                    : "d-none"
                                            }
                                        >
                                            <Link to="/inbox">
                                                <FontAwesomeIcon
                                                    style={{
                                                        color: "#ef004c",
                                                        fontSize: "32px",
                                                        margin: "-11px 12px"
                                                    }}
                                                    icon={faEnvelope}
                                                />
                                            </Link>
                                            <FontAwesomeIcon
                                                style={{
                                                    color: "#ef004c",
                                                    fontSize: "28px",
                                                    margin: "-10px 12px"
                                                }}
                                                icon={faBell}
                                            />

                                            <img
                                                className="rounded-circle"
                                                src={user.profileImage}
                                                alt="Profile Picture"
                                                style={{
                                                    width: "60px",
                                                    height: "60px",
                                                    margin: "0 12px",
                                                    border: "2px solid #fff",
                                                    background: "white"
                                                }}
                                            />

                                            <div className="btn-group">
                                                <a
                                                    href="#"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                >
                                                    <i
                                                        style={{
                                                            fontSize: "18px",
                                                            margin: "-5px 12px",
                                                            color: "white"
                                                        }}
                                                        id="profile-dropdown-trigger"
                                                        className="fas fa-chevron-down"
                                                    ></i>
                                                </a>

                                                <div className="dropdown-menu dropdown-menu-right profile-popup">
                                                    <button
                                                        className="dropdown-item"
                                                        type="button"
                                                    >
                                                        <Link to="/dashboard">
                                                            <i
                                                                style={{
                                                                    width:
                                                                        "2.4em"
                                                                }}
                                                                className="fa fa-tachometer"
                                                                aria-hidden="true"
                                                            ></i>
                                                            <span>
                                                                Dashboard
                                                            </span>
                                                        </Link>
                                                    </button>
                                                    <button
                                                        className="dropdown-item"
                                                        type="button"
                                                    >
                                                        <Link to="/my-donations">
                                                            <i
                                                                style={{
                                                                    width:
                                                                        "2.4em"
                                                                }}
                                                                className="fa fa-heartbeat"
                                                                aria-hidden="true"
                                                            ></i>
                                                            <span>
                                                                My Donations
                                                            </span>
                                                        </Link>
                                                    </button>
                                                    <Link to="/my-requests">
                                                        <button
                                                            className="dropdown-item"
                                                            type="button"
                                                        >
                                                            <i
                                                                style={{
                                                                    width:
                                                                        "2.4em",
                                                                    paddingLeft:
                                                                        "0.29em"
                                                                }}
                                                                className="fa fa-info"
                                                                aria-hidden="true"
                                                            ></i>
                                                            <span>
                                                                My Requests
                                                            </span>
                                                        </button>
                                                    </Link>

                                                    <button
                                                        className="dropdown-item"
                                                        type="button"
                                                    >
                                                        <Link to={`/profile/${user.username}`}>
                                                            <i
                                                                style={{
                                                                    width:
                                                                        "2.4em",
                                                                    paddingLeft:
                                                                        "2px"
                                                                }}
                                                                className="fa fa-user"
                                                                aria-hidden="true"
                                                            ></i>
                                                            <span>Profile</span>
                                                        </Link>
                                                    </button>
                                                    <button
                                                        className="dropdown-item"
                                                        type="button"
                                                    >
                                                        <Link to="/editprofile">
                                                            <i
                                                                style={{
                                                                    width:
                                                                        "2.4em",
                                                                    paddingLeft:
                                                                        "2px"
                                                                }}
                                                                className="fa fa-user"
                                                                aria-hidden="true"
                                                            ></i>
                                                            <span>
                                                                Edit Profile
                                                            </span>
                                                        </Link>
                                                    </button>
                                                    <button
                                                        className="dropdown-item"
                                                        type="button"
                                                    >
                                                        <Link to="/my-reviews">
                                                            <i
                                                                style={{
                                                                    width:
                                                                        "2.4em"
                                                                }}
                                                                className="fa fa-star"
                                                                aria-hidden="true"
                                                            ></i>
                                                            <span>Reviews</span>
                                                        </Link>
                                                    </button>
                                                    <ResetPassword />
                                                    <button
                                                        className="dropdown-item"
                                                        type="button"
                                                        onClick={
                                                            handleClick
                                                        }
                                                    >
                                                        <i
                                                            style={{
                                                                width: "2.8em"
                                                            }}
                                                            className="fa fa-sign-out"
                                                            aria-hidden="true"
                                                        ></i>
                                                        <span>Logout</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </span>
                                        <span
                                            className={
                                                isLoggedIn === true
                                                    ? "d-none"
                                                    : ""
                                            }
                                        >
                                            <LoginModal />
                                        </span>
                                    </li>
                                    <li style={{ paddingRight: 15 }}>
                                        <Link
                                            to="/createdonation"
                                            className="pc_text"
                                        >
                                            CREATE DONATIONS
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
};

const mapStateToProps = ({ user }) => ({
    isLoggedIn: user.isLoggedIn,
    user: user.credentials
});

const mapDispatchToProps = {
    userLogout
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
