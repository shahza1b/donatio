import React from "react";
import Spinner from "react-spinner-material";
export default function LoadingAnimation() {
    return (
        <div style={{ height: "100vh" }}>
            <div
                style={{
                    position: "absolute",
                    top: "50%",
                    left: "45%"
                }}>
                <Spinner
                    size={120}
                    spinnerColor={"#333"}
                    spinnerWidth={2}
                    visible={true}
                />
            </div>
        </div>
    );
}
