import React from "react";
import Modal from "react-bootstrap/Modal";
import { withFormik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import Spinner from "react-spinner-material";
import ForgotPassword from "./forgotPassword";
import { userLogin, userRegister } from "../Store/Actions";
import { connect, useDispatch, useSelector } from "react-redux";
import { SET_LOADING, CLEAR_ERRORS, SHOW_MODAL } from "../Store/consts";
import { withRouter } from "react-router";
import { setForm } from "../Store/Actions/uiActions";


const LoginForm = props => {
    const dispatch = useDispatch()
    const {
        Errors,
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        loading
    } = props;

    return (
        <>
            
                <form
                    id="loginForm"
                    className="login100-form validate-form p-l-15 p-r-15"
                    onSubmit={handleSubmit}
                >
                    {/* <span className="login100-form-title">Sign In</span> */}
                    <div
                        className="wrap-input100 validate-input m-b-16"
                        data-validate="Please enter username"
                    >
                        <input
                            required
                            className="input100"
                            type="text"
                            name="email"
                            placeholder="Email / Username"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email}
                        />

                        <ErrorMessage name="email" />
                    </div>
                    <div
                        className="wrap-input100 validate-input"
                        data-validate="Please enter password"
                    >
                        <input
                            required
                            className="input100"
                            type="password"
                            name="password"
                            placeholder="Password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.password}
                        />
                        {errors.password && touched.password ? (
                            <div className="alert alert-danger mb-0 mt-3">
                                <ErrorMessage name="password" />
                            </div>
                        ) : null}

                        {Errors && (
                            <div className="alert alert-danger mb-0 mt-3">
                                {Errors.general}
                            </div>
                        )}
                    </div>
                    <div className="container-login100-form-btn p-t-20">
                        <button
                            type="submit"
                            className="login100-form-btn"
                            style={{
                                width: "100%",
                                background: "#41B541"
                            }}
                            disabled={loading}
                        >
                            {isSubmitting ? (
                                <Spinner
                                    size={20}
                                    spinnerColor={"#fff"}
                                    spinnerWidth={2}
                                    visible={true}
                                />
                            ) : (
                                "SIGN IN"
                            )}
                        </button>
                    </div>
                    <div className="text-right p-t-13 p-b-23">
                            <span
                                style={{ color: "#ef004c !important", cursor: "pointer" }}
                                onClick={() => dispatch(setForm("FORGOT PASSWORD"))}
                                className="txt2"
                            >
                                Forgot Password
                            </span>
                    </div>
                    <div className="irs-section-title p-15 m-0">
                        <div
                            className="irs-title-line"
                            style={{ width: "90%" }}
                        >
                            <div
                                className="irs-title-icon"
                                style={{ color: "grey", left: "45%" }}
                            >
                                OR
                            </div>
                        </div>
                    </div>
                    <div className="row" style={{ paddingLeft: "3%" }}>
                        <div className="col-md-4 p-0">
                            <img src="../images/fb_icon.png" />
                        </div>
                        <div className="col-md-4 p-0">
                            <img src="../images/twitter_icon.png" />
                        </div>
                        <div className="col-md-4 p-0">
                            <img src="../images/gplus_icon.png" />
                        </div>
                    </div>
                </form>
            
        </>
    );
};

const RegisterForm = props => {
    const {
        Errors,
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        loading
    } = props;

    return (
        <form
            id="loginForm"
            className="login100-form validate-form p-l-15 p-r-15"
            onSubmit={handleSubmit}
        >
            {/* <span className="login100-form-title">Sign In</span> */}
            <div className="wrap-input100 validate-input m-b-16">
                <input
                    required
                    className="input100"
                    type="text"
                    name="username"
                    placeholder="Username"
                    onChange={handleChange}
                    value={values.username}
                    onBlur={handleBlur}
                    minLength={5}
                />
            </div>
            <div className="wrap-input100 validate-input m-b-16">
                <input
                    required
                    className="input100"
                    type="text"
                    name="name"
                    placeholder="Full Name"
                    onChange={handleChange}
                    value={values.name}
                    onBlur={handleBlur}
                />
            </div>
            <div className="wrap-input100 validate-input m-b-16">
                <input
                    required
                    className="input100"
                    type="email"
                    name="email"
                    placeholder="Email Address"
                    onChange={handleChange}
                    value={values.email}
                    onBlur={handleBlur}
                />
            </div>
            <div className="wrap-input100 validate-input">
                <input
                    required
                    className="input100"
                    type="password"
                    name="password"
                    placeholder="Password"
                    onChange={handleChange}
                    value={values.password}
                    onBlur={handleBlur}
                />

                {errors.password && touched.password ? (
                    <div className="alert alert-danger mb-0 mt-3">
                        <ErrorMessage name="password" />
                    </div>
                ) : null}
            </div>
            {Errors ? (
                <div className="alert alert-info mb-0 mt-3">
                    {Errors.message}
                </div>
            ) : null}
            <div className="container-login100-form-btn p-t-20">
                <button
                    type="submit"
                    style={{ width: "100%", background: "#41B541" }}
                    className="login100-form-btn"
                    disabled={loading}
                >
                    {loading ? (
                        <Spinner
                            size={20}
                            spinnerColor={"#fff"}
                            spinnerWidth={2}
                            visible={true}
                        />
                    ) : (
                        "REGISTER"
                    )}
                </button>
            </div>

            <div className="irs-section-title p-t-30 p-15 m-0">
                <div className="irs-title-line" style={{ width: "90%" }}>
                    <div
                        className="irs-title-icon"
                        style={{
                            color: "grey",
                            left: "32%",
                            width: "118px"
                        }}
                    >
                        or Login with
                    </div>
                </div>
            </div>
            <div className="row" style={{ paddingLeft: "3%" }}>
                <div className="col-md-4 p-0">
                    <img src="../images/fb_icon.png" />
                </div>
                <div className="col-md-4 p-0">
                    <img src="../images/twitter_icon.png" />
                </div>
                <div className="col-md-4 p-0">
                    <img src="../images/gplus_icon.png" />
                </div>
            </div>
        </form>
    );
};

const Login_Modal = (props) => {
    const dispatch = useDispatch()
    const form = useSelector(state => state.Modal.form)
    const show = useSelector(state => state.Modal.show)
    const setShow = (view) => {
        dispatch({ 
            type: SHOW_MODAL,
            payload: view
        })
    }
    React.useEffect(() => {
        dispatch({ 
            type: SHOW_MODAL,
            payload: false
        })
    },[props.location])
    return (
        <>
            <span style={{cursor: "pointer " }} className="login_button" onClick={() => setShow(true)}>
                LOGIN
            </span>

            <Modal
                show={show}
                onHide={() => setShow(false)}
                dialogClassName="modal-10w"
                aria-labelledby="example-custom-modal-styling-title"
            >
                <Modal.Header closeButton>
                    {form !== "LOGIN" ? (
                        <button
                            onClick={() => dispatch(setForm("LOGIN"))}
                            className="p-t-5 p-r-15"
                        >
                            <FontAwesomeIcon icon={faChevronLeft} />
                        </button>
                    ) : null}
                    <Modal.Title id="forgot-password-modal-title">
                        {form}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {form == "LOGIN" ? (
                        <>
                            <Login />

                            <div className="flex-col-c p-t-30 p-b-20">
                                <span className="txt1 p-b-9">New user?</span>
                                <span
                                    onClick={() => dispatch(setForm("REGISTER"))}
                                    style={{
                                        color: "#ef004c",
                                        cursor: "pointer "
                                    }}
                                >
                                    REGISTER NOW
                                </span>
                            </div>
                        </>
                    ) : form == "REGISTER" ? (
                        <Register />
                    ) : form == "FORGOT PASSWORD" ? (
                        <ForgotPassword />
                    ) : null
                    }
                    
                </Modal.Body>
            </Modal>
        </>
    );
};

const validationSchema = Yup.object().shape({
    // name: Yup.string().required("Name Required"),
    email: Yup.string().required("Email Required"),
    password: Yup.string().min(6, "Minimum 6 characters long").required("Password Required"),
    // username: Yup.string().min(5, "Minimum 5 characters long").required("Username Required")
});

const Login_Form = withRouter(withFormik({
    mapPropsToValues: () => ({ email: "", password: "" }),
    validationSchema,
    validateOnBlur: 1,
    handleSubmit: async (values, { props, setSubmitting }) => {
        props.dispatch({ type: SET_LOADING });
        props.dispatch({ type: CLEAR_ERRORS })
        setTimeout(async () => {
            await props.dispatch(userLogin(values, props.history));
            setSubmitting(false);
        }, 1000);
    },

    displayName: "BasicForm"
})(connect(mapStateToProps)(LoginForm)));

const Register_Form = withRouter(withFormik({
    mapPropsToValues: () => ({
        username: "",
        name: "",
        email: "",
        password: ""
    }),
    validationSchema,
    validateOnBlur: 1,
    handleSubmit: (values, { props, setSubmitting }) => {
        props.dispatch({ type: CLEAR_ERRORS })
        props.dispatch({ type: SET_LOADING });
        setTimeout(async () => {
            await props.dispatch(userRegister(values, props.history));
            setSubmitting(false);
        }, 1000);
    },

    displayName: "RegisterForm"
})(connect(mapStateToProps)(RegisterForm)))

const Login = connect(mapStateToProps)(Login_Form);
const Register = connect(mapStateToProps)(Register_Form);

function mapStateToProps(state) {
    return {
        user: state.user,
        Errors: state.UI.errors,
        loading: state.UI.loading
    };
}
// connect(mapStateToProps)(LoginForm);
export default withRouter(Login_Modal);
