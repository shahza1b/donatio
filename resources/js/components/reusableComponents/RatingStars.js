import React from "react";
import StarRatingComponent from "react-star-rating-component";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faHeart } from "@fortawesome/free-solid-svg-icons";

//Custom Props for StarRating Component
// name: "name" important
// stars: 'numeric' number of stars
// rating: 'numeric string' rating for stars

class RatingStars extends React.Component {
    constructor() {
        super();

        this.state = {
            rating: 5
        };
    }

    componentDidMount() {
        this.setState({
            rating: this.props.rating ? parseInt(this.props.rating) : 5
        });
    }
    onStarClick(nextValue, prevValue) {
        this.setState({ rating: nextValue });
    }

    render() {
        const { rating } = this.state;

        return (
            <div
                style={{
                    height: this.props.height,
                    paddingRight: this.props.paddingRight
                }}>
                <StarRatingComponent
                    name={this.props.name ? this.props.name : "rate1"}
                    starCount={
                        this.props.stars ? parseInt(this.props.stars) : 5
                    }
                    editing={this.props.editing == "true" ? true : false}
                    value={rating}
                    onStarClick={this.onStarClick.bind(this)}
                    starColor={
                        this.props.color ? this.props.color : "rgb(255, 180, 0)"
                    }
                    renderStarIcon={() => {
                        if (this.props.font) {
                            return (
                                <i
                                    style={{
                                        fontStyle: "normal",
                                        fontSize: "2em"
                                    }}>
                                    {this.props.font}
                                </i>
                            );
                        } else {
                            return <i style={{ fontStyle: "normal" }}>★</i>;
                        }
                    }}
                />

                {this.props.details ? (
                    <span
                        style={{
                            position: "absolute",
                            right: this.props.right
                        }}>
                        {rating == "5" ? rating : rating + ".0"}
                    </span>
                ) : null}
            </div>
        );
    }
}

export default RatingStars;
