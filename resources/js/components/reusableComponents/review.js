import React from "react";
import RatingStars from "./RatingStars";

const Review = props => {
    return (
        <div className="container">
            <div className="Review-section">
                <div
                    className="col-md-12"
                    style={{
                        background: "#ece9e98a",
                        borderRadius: "20px",
                        paddingRight: "1px",
                        height: "15em",
                        paddingTop: "1em"
                        // height: "15em"
                    }}
                >
                    <div
                        id="scroll-bar"
                        style={{
                            height: "13em",
                            overflowY: "scroll",
                            overflowX: "hidden",
                            padding: "1.5em",
                            paddingTop: "0.5em"
                        }}
                    >
                        <div
                            style={{
                                fontSize: "1.2em",
                                color: "#00000075",
                                paddingBottom: "1em"
                            }}
                        >
                            Rate and Review
                        </div>
                        <div className="row">
                            <div
                                className={
                                    props.profile ? "col-md-8" : "col-md-9"
                                }
                            >
                                <img
                                    src="/images/donors_icon.png"
                                    className="rounded mx-auto d-block float-left p-r-15"
                                    alt="Profile Image"
                                    style={{ width: "70px" }}
                                />
                                <h4>Auther Name</h4>
                                <h6
                                    style={{
                                        color: "grey",
                                        fontSize: "15px",
                                        paddingTop: "5px"
                                    }}
                                >
                                    Receiver
                                </h6>
                            </div>
                            <div
                                className={
                                    props.profile
                                        ? "col-md-4 p-t-2"
                                        : "col-md-3 p-t-2"
                                }
                                style={{ textAlign: "end" }}
                            >
                                <i className="fa fa-map-marker">
                                    &nbsp;&nbsp; Gujranwala, Pakistan
                                </i>
                                <div style={{ clear: "both" }}></div>
                                <RatingStars
                                    right="5%"
                                    paddingRight="1em"
                                    color="#ef004c"
                                    details="true"
                                />
                            </div>
                            <div
                                className="col-md-12 mt-2"
                                style={{ fontSize: "0.9em" }}
                            >
                                Lorem Ipsum is simply dummy text of the printing
                                and typesetting industry. Lorem Ipsum has been
                                the industry's standard dummy text ever since
                                the 1500s, when an unknown printer took a galley
                                of type and scrambled it to make a type specimen
                                book. It has survived not only five centuries,
                                but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was
                                popularised in the 1960s with the release of
                                Letraset sheets containing Lorem Ipsum passages,
                                and more recently with desktop publishing
                                software like Aldus PageMaker including versions
                                of Lorem Ipsum. Lorem Ipsum is simply dummy text
                                of the printing and typesetting industry. Lorem
                                Ipsum has been the industry's standard dummy
                                text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it
                                to make a type specimen book. It has survived
                                not only five centuries, but also the leap into
                                electronic typesetting, remaining essentially
                                unchanged. It was popularised in the 1960s with
                                the release of Letraset sheets containing Lorem
                                Ipsum passages, and more recently with desktop
                                publishing software like Aldus PageMaker
                                including versions of Lorem Ipsum.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Review;
