import React, { useEffect, Fragment, useState } from "react";
import RatingStars from "./RatingStars";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import CurrentDonations from "./CurrentDonations";
import Review from "./review";
import { useSelector, useDispatch } from "react-redux";
import { setUserData } from "../Store/Actions";
import { setDonorProfile } from "../Store/Actions/donorsActions";
import _404 from './../404/index';
import LoadingAnimation from "./LoadingAnimation";
import { LOADING_DONOR_PROFILE } from './../Store/consts';


const Profile = (props) => {
    // const [data, setData] = useState[{}]
    const dispatch = useDispatch();
    // const user = useSelector(state => state.user.credentials);
    // const isLoggedIn = useSelector(state => state.user.isLoggedIn);
    const data = useSelector(state => state.donors.profile)
    const loading = useSelector(state => state.donors.loading)
    // useEffect(() => {
    //     if (isLoggedIn) {
    //         dispatch(setUserData());
    //     }
    // }, []);



    const fetchProfile = (username) => {
        dispatch({ type: LOADING_DONOR_PROFILE })
        dispatch(setDonorProfile(username));
        return;
    }

    useEffect(() => {
        fetchProfile(props.match.params.username)
    },[props.match.params.username])

    // setData(Profile)

    return (
        <Fragment>
            {
                loading? <LoadingAnimation /> : data != "Not Found"  ? <Fragment>
                {/* <a href="#"> */}
                <div>
                    <div
                        style={{
                            height: "130px",
                            backgroundColor: "#ef004c",
                            width: "100%",
                            marginBottom: "70px"
                        }}
                    >
                        <div
                            style={{
                                position: "relative",
                                top: "34%",
                                left: "44%"
                            }}
                        >
                            <span
                                style={{
                                    width: "160px",
                                    height: "160px",
                                    position: "absolute",
                                    zIndex: "999",
                                    border: "4px solid white",
                                    borderRadius: "50%",
                                    boxShadow: "0px 22px 9px #0000001c",
                                    marginBottom: "70px",
                                    background: "white"
                                }}
                            >
                                <img
                                    src={data&&data.profileImage}
                                    className="rounded-circle mx-auto d-block"
                                    alt="Profile Image"
                                    style={{
                                        width: "100%",
                                        height: "100%"
                                    }}
                                />
                            </span>
                            {/* <i
                                className="fas fa-camera"
                                style={{
                                    position: "relative",
                                    top: "20px",
                                    left: "127px",
                                    zIndex: "999",
                                    border: "2px solid #ef004c",
                                    color: "#ef004c",
                                    fontSize: "19px",
                                    borderRadius: "50%",
                                    background: "white",
                                    padding: "8px"
                                }}
                            ></i> */}
                        </div>
                    </div>
                </div>
                <h2 className="text-center pt-4" style={{ color: "black" }}>
                    {data && data.name}
                </h2>
                <i className=" d-flex  justify-content-center pt-2">
                    &nbsp;&nbsp; {data ? data.location : "Location"}
                </i>
                <div className="text-center pt-2">
                    <RatingStars />
                </div>
            {/* </a> */}
            <div className="container">
                <div className="row">
                    <div className="text-center col-md-8 " style={{ margin: "0px auto" }}>
                        <span className="justify-content-center">
                            {data && data.about}
                        </span>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row mt-4">
                    <div className="col-md-2"></div>
                    <div className="col-md-8 justify-content-center m-b-90">
                        <div
                            className="col-md-12"
                            style={{
                                padding: "15px 0 20px 0"
                            }}
                        >
                            <Tabs
                                defaultActiveKey="available_donations"
                                id="donation-tabs"
                            >
                                <Tab
                                    eventKey="available_donations"
                                    title="Current Donations"
                                    style={{
                                        height: "70vh",
                                        overflow: "hidden scroll"
                                    }}
                                >
                                    {
                                        data && data.donations && data.donations.map(item =>
                                            <CurrentDonations 
                                            key={item.id}
                                            button="current" 
                                            id={item.id}
                                            title={item.title}
                                            description={item.description}
                                            images={item.images}
                                            url={item.url}
                                            username={data.username}
                                            />
                                            )
                                    }
                                </Tab>
                                <Tab
                                    eventKey="donated_items"
                                    title="Donation History"
                                    style={{
                                        height: "70vh",
                                        overflow: "hidden scroll"
                                    }}
                                >
                                     {
                                        data && data.donated && data.donated.map(item =>
                                            <CurrentDonations 
                                            key={item.id}
                                            button="current" 
                                            id={item.id}
                                            title={item.title}
                                            description={item.description}
                                            images={item.images}
                                            url={item.url}
                                            username={data.username}
                                            />
                                            )
                                    }
                                </Tab>
                                <Tab
                                    eventKey="donated_item"
                                    title="Rate & Review"
                                >
                                    <Review profile="1" />
                                </Tab>
                            </Tabs>
                            {/* padding: 15px 0 20px 0; */}
                        </div>
                    </div>
                    <div className="col-md-2"></div>
                </div>
            </div>
        
            </Fragment>
            : <_404 title="Profile" />
            }
            </Fragment>
    );
};

export default Profile;
