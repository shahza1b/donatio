import React, { Fragment, Component } from "react";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import LatestDonationComponent from "./latestDonationComponent";
import ProductDetailGallery from "./ProductDetailGallery";
import RatingStars from "./RatingStars";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faShare,
    faHandPointUp,
    faHeart,
    faChevronLeft,
    faChevronRight
} from "@fortawesome/free-solid-svg-icons";
import ReportPostModal from "./reportPostModal";
import Review from "./review";
import LoadingAnimation from "./LoadingAnimation";

import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
dayjs.extend(relativeTime);

import { connect } from "react-redux";
import { setPost } from "./../Store/Actions/postActions";
import _404 from "../404";

class DonatedItemDetail extends Component {
    componentDidMount() {
        // console.log(this.props)
        this.props.setPost(
            this.props.match.params.username,
            this.props.match.params.title
        );
    }

    async componentWillReceiveProps(nextProps){
        if(this.props.match != nextProps.match){
            await this.props.setPost(
                nextProps.match.params.username,
                nextProps.match.params.title
            )
            window.scrollTo({
                top: 100,
                behavior: 'smooth'
              })
        }
    }
    render() {
        const { loading, post, user, more, related } = this.props;
        return (
            <Fragment>

                { loading ? <LoadingAnimation /> : !loading && post.user && post.images ? (
                    <Fragment>
                        <div className="content_section">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6 col-sm-12 post_title">
                                        <h3 className="mb-2">{post.title}</h3>
                                        <p className="text-muted">
                                            <i
                                                className="fa fa-map-marker"
                                                aria-hidden="true"
                                            />{" "}
                                            &nbsp;
                                            {post.city + " " + post.country}
                                            &nbsp;
                                        </p>
                                    </div>
                                    <div className="col-md-6 col-sm-12 text-right">
                                        <p className="social_icons">
                                            <a
                                                href="#"
                                                style={{ padding: "0px 6px" }}
                                            >
                                                <i
                                                    className="fa fa-facebook-square"
                                                    aria-hidden="true"
                                                />
                                            </a>
                                            <a
                                                style={{ padding: "0px 6px" }}
                                                href="#"
                                            >
                                                <i
                                                    className="fa fa-twitter-square"
                                                    aria-hidden="true"
                                                />
                                            </a>
                                            <a
                                                style={{ padding: "0px 6px" }}
                                                href="#"
                                            >
                                                <i
                                                    className="fa fa-pinterest-square"
                                                    aria-hidden="true"
                                                />
                                                {/* <i className="fa fa-pinterest"></i> */}
                                            </a>
                                        </p>
                                        <ReportPostModal />
                                    </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-md-6 col-sm-12 post_img p-t-6">
                                        <ProductDetailGallery
                                            data={post.images}
                                        />
                                    </div>
                                    <div className="col-md-6 col-sm-12 post_desc">
                                        <span>
                                            <i
                                                className="fa fa-clock-o"
                                                aria-hidden="true"
                                            />
                                            &nbsp;{" "}
                                            {dayjs(post.created_at).fromNow()}
                                        </span>
                                        <p
                                            id="scroll-bar"
                                            style={{
                                                height: "7em",
                                                overflowY: "scroll"
                                            }}
                                        >
                                            {post.description}
                                        </p>
                                        <br />
                                        <div className="post_info">
                                            <div className="row">
                                                <div className="col-md-5">
                                                    <h5>Approximate Value:</h5>
                                                </div>
                                                <div className="col-md-7">
                                                    {post.approx_value}
                                                </div>
                                                <div className="col-md-5">
                                                    <h5>Product Condition:</h5>
                                                </div>
                                                <div className="col-md-7">
                                                    <span className="focused-tab">
                                                        {post.product_condition}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-2 d-inline-flex p-t-16">
                                                <FontAwesomeIcon
                                                    style={{
                                                        color:
                                                            "rgb(239, 0, 76)",
                                                        fontSize: "1.4em"
                                                    }}
                                                    icon={faHeart}
                                                />
                                                <span className=" p-l-17">
                                                    {post.hearts}
                                                </span>
                                                {/* color="rgb(239, 0, 76)" */}
                                            </div>
                                            <div className="col-md-2 d-inline-flex p-t-16">
                                                <FontAwesomeIcon
                                                    style={{
                                                        color: "#11c0c3",
                                                        fontSize: "1.4em"
                                                    }}
                                                    icon={faShare}
                                                />
                                                <span className="p-l-17">
                                                    {post.shares}
                                                </span>
                                            </div>
                                            <div className="col-md-2 d-inline-flex p-t-16">
                                                <FontAwesomeIcon
                                                    style={{
                                                        color: "#3eb53f",
                                                        fontSize: "1.4em"
                                                    }}
                                                    icon={faHandPointUp}
                                                />
                                                <span className=" p-l-17">
                                                    {" "}
                                                    {post.views}{" "}
                                                </span>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="col-md-12">
                                            <a href="#">
                                                <img
                                                    src={post.user.profileImage}
                                                    className="rounded roundImage mx-auto d-block float-left mr-2"
                                                    alt="Profile Image"
                                                    style={{
                                                        width: "55px",
                                                        height: "55px"
                                                    }}
                                                />
                                                <h3 style={{ color: "black" }}>
                                                    {post.user.name}
                                                </h3>
                                                <h6
                                                    style={{
                                                        paddingBottom: ".5em"
                                                    }}
                                                >
                                                    Donor
                                                </h6>
                                                <div className="row">
                                                    <div
                                                        style={{
                                                            fontSize: "1.3em"
                                                        }}
                                                        className="col-md-6"
                                                    >
                                                        <RatingStars
                                                            editing="true"
                                                            right="54%"
                                                            color="#ef004c"
                                                            height="1.7em"
                                                            rating="1"
                                                            details="true"
                                                        />
                                                    </div>
                                                    <div
                                                        className="col-md-6"
                                                        style={{
                                                            paddingTop: 10
                                                        }}
                                                    >
                                                        <div
                                                            style={{
                                                                float: "right"
                                                            }}
                                                        >
                                                            <i className="fa fa-map-marker float-right">
                                                                &nbsp;&nbsp;{" "}
                                                                {
                                                                    post.user
                                                                        .location
                                                                }
                                                            </i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <i className="fas fa-comment-alt ">
                                                            &nbsp;&nbsp; Send
                                                            message
                                                        </i>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div
                                                            style={{
                                                                float: "right"
                                                            }}
                                                        >
                                                            <FontAwesomeIcon
                                                                style={{
                                                                    color:
                                                                        "rgb(239, 0, 76)",
                                                                    fontSize:
                                                                        "1em"
                                                                }}
                                                                icon={faHeart}
                                                            />
                                                            <span className=" p-l-5 font-weight-bold">
                                                                1K
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        {/* <a class="text-primary" href="#" data-toggle="modal" data-target="#mynModal">asdfasfds</a> */}
                                        {user.credentials &&
                                            !(
                                                user.credentials.id ===
                                                post.user.id
                                            ) && (
                                                <button
                                                    className="btn-block btn-lg"
                                                    data-toggle="modal"
                                                    data-target="#emailpopup"
                                                    style={{
                                                        border:
                                                            "1px solid transparent",
                                                        borderRadius: "25px",
                                                        color: "white",
                                                        background:
                                                            "rgb(65, 181, 65)",
                                                        marginTop: ".8em"
                                                    }}
                                                >
                                                    Request Now
                                                </button>
                                            )}
                                    </div>
                                </div>
                                <br />
                            </div>
                        </div>
                        <hr />
                        <Review />
                    <Fragment>
                    <div className="container">
                        <div className="row">
                            <div
                                className="col-md-12"
                                style={{
                                    padding: "15px 0 20px 0"
                                }}
                            >
                                <Tabs
                                    defaultActiveKey="posts-from-this-user"
                                    id="donation-tabs"
                                >
                                    <Tab
                                        eventKey="posts-from-this-user"
                                        title="More Posts from this user"
                                    >
                                        { more.length > 0 ?
                                            more
                                                .slice(0, 3)
                                                .map((post) => (
                                                    <LatestDonationComponent
                                                        key={post.id}
                                                        image={post.images[0].original}
                                                        date={post.created_at}
                                                        location={post.city + " " + post.country}
                                                        description={post.description.slice(0,74) + "..."}
                                                        price={post.approx_value}
                                                        requests={post.request? post.requests: null}
                                                        authorName={post.user && post.user.name }
                                                        authorImage={post.user && post.user.profileImage}
                                                        username={post.user && post.user.username}
                                                        rating={post.rating? post.rating: "0"}
                                                        url={post.url}
                                                    />
                                                )) : 
                                                <div className="notFound">
                                                    <div
                                                    className="notFound alert alert-danger alert-dismissible fade show ml-3 mt-2"
                                                    role="alert"
                                                    style={{
                                                        display: "inline-block",
                                                        margin: "0 auto",
                                                        width: "50%",
                                                        textAlign: "center",
                                                    }}
                                                    >
                                                    No Other Donations by this User
                                                        </div>
                                                </div>
                                                }
                                    </Tab>
                                    <Tab
                                        eventKey="related-posts"
                                        title="Related Posts"
                                    >
                                        {related.length > 0 ?
                                            related
                                                .slice(0, 3)
                                                .map((post) => (
                                                    <LatestDonationComponent
                                                        key={post.id}
                                                        image={post.images[0].original}
                                                        date={post.created_at}
                                                        location={post.city + " " + post.country}
                                                        description={post.description.slice(0,74) + "..."}
                                                        price={post.approx_value}
                                                        requests={post.request? post.requests: null}
                                                        authorName={post.user && post.user.name }
                                                        authorImage={post.user && post.user.profileImage}
                                                        username={post.user && post.user.username}
                                                        rating={post.rating? post.rating: "0"}
                                                        url={post.url}
                                                    />
                                                )): 
                                                <div className="notFound">
                                                    <div
                                                    className="alert alert-danger alert-dismissible fade show ml-3 mt-2"
                                                    role="alert"
                                                    style={{
                                                        display: "inline-block",
                                                        margin: "0 auto",
                                                        width: "50%",
                                                        textAlign: "center",
                                                    }}
                                                    >
                                                    No Related Donations Found
                                                        </div>
                                                </div>
                                                }
                                    </Tab>
                                </Tabs>
                                {/* padding: 15px 0 20px 0; */}
                            </div>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row row-centered mb-5 mt-2">
                            <div className="col-md-12">
                                <span
                                    className="button-hover-style"
                                    style={{
                                        color: "rgb(239, 0, 76)",
                                        border: "1px solid rgb(239, 0, 76)",
                                        borderRadius: "50%",
                                        padding: "15px 19px"
                                    }}
                                >
                                    <FontAwesomeIcon icon={faChevronLeft} />
                                </span>
                                <button
                                    className="button-hover-style"
                                    style={{
                                        color: "#ef004c",
                                        border: "1px solid #ef004c",
                                        borderRadius: "25px",
                                        padding: "12px 28px",
                                        margin: "0px 12px"
                                    }}
                                >
                                    VIEW ALL
                                </button>
                                <span
                                    className="button-hover-style"
                                    style={{
                                        color: "rgb(239, 0, 76)",
                                        border: "1px solid rgb(239, 0, 76)",
                                        borderRadius: "50%",
                                        padding: "15px 19px"
                                    }}
                                >
                                    <FontAwesomeIcon
                                        icon={faChevronRight}
                                    />
                                </span>
                            </div>
                        </div>
                    </div>
                
                    </Fragment>
                    
                    </Fragment>
                ) : !loading && post === "Post Not Found" ? (
                    <_404 title="Post" />
                ) : null}
            </Fragment>
        );
    }
}

const mapStateToProps = ({ user, latestPosts }) => ({
    user: user,
    loading: latestPosts.loading,
    post: latestPosts.post,
    more: latestPosts.moreFromThisUser,
    related: latestPosts.relatedPosts
});

const mapDispatchToProps = {
    setPost
};

export default connect(mapStateToProps, mapDispatchToProps)(DonatedItemDetail);
