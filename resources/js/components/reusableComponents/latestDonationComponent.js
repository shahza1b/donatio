import React from "react";
import RatingStars from "./RatingStars";
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { connect } from "react-redux";
 
dayjs.extend(relativeTime);


const LatestDonationComponent = props => {
    const {
        image,
        date,
        location,
        description,
        price,
        requests,
        authorName,
        authorImage,
        username,
        rating,
        url, 
        user
    } = props;
    const donationURL = "/details/" + url
    return (
        <div className="col-lg-4 col-md-6 col-sm-12 col-centered mt-4">
            <div className="donation-card">
                <Link to={donationURL}>
                    <div className="irs-campaign-col ">
                        <div className="irs-campaign-img">
                            <img
                                src={
                                    image
                                        ? image
                                        : "https://donatio.love/storage/app/images/37-15421084240.jpg"
                                }
                                className="img-responsive"
                                width="100%"
                            />

                            <div className="irs-campaign-img-text">
                                <ul className="clearfix">
                                    <li>
                                        <p>
                                            <i className="fa fa-clock" />
                                            { date ? dayjs(date).fromNow() : "9 months ago"}
                                        </p>
                                    </li>
                                    <li
                                        style={{ float: "right" }}
                                        className="card-city"
                                    >
                                        <p>
                                            <i className="fa fa-map-marker" />
                                            &nbsp;&nbsp;
                                            {location
                                                ? location
                                                : "Gujranwala, Pakistan"}
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="irs-campaign-content">
                            <p className="title">
                                {description
                                    ? description
                                    : "Lorem ipsum is simply dummy text of the printing and typesetting industry..."}
                            </p>
                            <div className="row p-t-10">
                                <div className="col p-l-0">
                                    <p>
                                        <strong
                                            style={{
                                                color: "grey",
                                                fontWeight: "500"
                                            }}
                                        >
                                            Approx. value
                                        </strong>
                                        <br />
                                        <span
                                            style={{
                                                color: "#EF004C"
                                            }}
                                        >
                                            {price ? price : "0"}
                                        </span>
                                    </p>
                                </div>
                                <div className="col p-r-0 ">
                                    <p className="float-right">
                                        <strong
                                            style={{
                                                color: "grey",
                                                fontWeight: "500"
                                            }}
                                        >
                                            Requests
                                        </strong>
                                        <br />
                                        <span
                                            style={{
                                                color: "#EF004C",
                                                float: "right"
                                            }}
                                        >
                                            {requests ? requests : "200"}
                                        </span>
                                    </p>
                                </div>
                            </div>
                            <hr />
                            <div className="row p-t-12">
                                <div
                                    className="col-2"
                                    style={{ padding: "5px" }}
                                >
                                    <img
                                        // className="roundImage"
                                        style={{borderRadius: '50%', height: 43}}
                                        src={authorImage}
                                        alt="Profile Image"
                                        width="100%"
                                    />
                                </div>
                                <div className="col-5 ">
                                    <span
                                        style={{
                                            fontSize: "18px",
                                            fontWeight: '400'
                                        }}
                                    >
                                        {authorName
                                            ? authorName
                                            : "Author Name"}
                                    </span>
                                    <br />
                                    <RatingStars rating={rating!=0? rating: '0'} />
                                </div>
                                { (username !== user.username) && 
                                    <div
                                    className="col-5"
                                    style={{
                                        padding: "5px 0 0 0"
                                    }}
                                >
                                    <button
                                        className="btn btn-outline-success"
                                        style={{
                                            borderRadius: "25px",
                                            padding: "12px 15px",
                                            fontSize: '16px !important'
                                        }}
                                    >
                                        Request Now
                                    </button>
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </Link>
            </div>
        </div>
    );
};


const mapStateToProps = (state) => ({
    user: state.user.credentials
})

export default connect(mapStateToProps)(LatestDonationComponent);

LatestDonationComponent.defaultProps = {
    image: "https://miro.medium.com/max/1280/1*-GsJA8f7J8VrL8RouKCjQw.jpeg",
    date: false,
    location: false,
    description: false,
    price: false,
    requests: false,
    authorName: false,
    rating: 0,
};
