import React from "react";
import Modal from "react-bootstrap/Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { withFormik } from "formik";
import { userRegister } from "../Store/Actions";
import { connect } from "react-redux";
// import "./css/main.css";

class RegisterForm extends React.Component {
    state = { errors: "" };
    componentDidMount() {
        let appState = {
            isLoggedIn: false,
            user: {},
            errors: ""
        };
        // save app state with user date in local storage
        localStorage["appState"] = JSON.stringify(appState);
    }
    handleClick = () => {
        setTimeout(() => {
            let data = localStorage.appState
                ? JSON.parse(localStorage.appState)
                : { errors: "" };
            this.setState({
                errors: data.errors != "" ? data.errors : ""
            });
        }, 3000);
    };
    render() {
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
            handleSubmit
        } = this.props;

        return (
            <form
                id="loginForm"
                className="login100-form validate-form p-l-15 p-r-15"
                onSubmit={handleSubmit}>
                {/* <span className="login100-form-title">Sign In</span> */}
                <div className="wrap-input100 validate-input m-b-16">
                    <input
                        required
                        className="input100"
                        type="text"
                        name="username"
                        placeholder="Username"
                        onChange={handleChange}
                        value={values.username}
                        onBlur={handleBlur}
                    />
                </div>
                <div className="wrap-input100 validate-input m-b-16">
                    <input
                        required
                        className="input100"
                        type="text"
                        name="name"
                        placeholder="Full Name"
                        onChange={handleChange}
                        value={values.name}
                        onBlur={handleBlur}
                    />
                </div>
                <div className="wrap-input100 validate-input m-b-16">
                    <input
                        required
                        className="input100"
                        type="email"
                        name="email"
                        placeholder="Email Address"
                        onChange={handleChange}
                        value={values.email}
                        onBlur={handleBlur}
                    />
                </div>
                <div className="wrap-input100 validate-input">
                    <input
                        required
                        className="input100"
                        type="password"
                        name="password"
                        placeholder="Password"
                        onChange={handleChange}
                        value={values.password}
                        onBlur={handleBlur}
                    />
                </div>
                {this.state.errors == "" ? null : (
                    <div className="alert alert-danger mb-0 mt-3">
                        {this.state.errors}
                    </div>
                )}
                <div className="container-login100-form-btn p-t-20">
                    <button
                        type="submit"
                        onClick={this.handleClick}
                        style={{ width: "100%", background: "#41B541" }}
                        className="login100-form-btn">
                        JOIN US NOW
                    </button>
                </div>

                <div className="irs-section-title p-t-30 p-15 m-0">
                    <div className="irs-title-line" style={{ width: "90%" }}>
                        <div
                            className="irs-title-icon"
                            style={{
                                color: "grey",
                                left: "32%",
                                width: "118px"
                            }}>
                            or Login with
                        </div>
                    </div>
                </div>
                <div className="row" style={{ paddingLeft: "3%" }}>
                    <div className="col-md-4 p-0">
                        <img src="../images/fb_icon.png" />
                    </div>
                    <div className="col-md-4 p-0">
                        <img src="../images/twitter_icon.png" />
                    </div>
                    <div className="col-md-4 p-0">
                        <img src="../images/gplus_icon.png" />
                    </div>
                </div>
            </form>
        );
    }
}

const Register_Modal = (props) => {
    const [show, setShow] = React.useState(false);

    return (
        <>
            <a
                style={{ color: "#ef004c" }}
                onClick={() => setShow(true)}
                href="#"
                className="txt3 ">
                {props.title}
            </a>
            <Modal
                show={show}
                onHide={() => setShow(false)}
                backdrop={false}
                dialogClassName="modal-10w"
                aria-labelledby="example-custom-modal-styling-title">
                <Modal.Header closeButton>
                    <button
                        onClick={() => setShow(false)}
                        className="p-t-5 p-r-15">
                        <FontAwesomeIcon icon={faChevronLeft} />
                    </button>
                    <Modal.Title id="forgot-password-modal-title">
                        REGISTER
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Register />
                </Modal.Body>
            </Modal>
        </>
    );
};

const RegisterModal = withFormik({
    mapPropsToValues: () => ({
        username: "",
        name: "",
        email: "",
        password: ""
    }),

    handleSubmit: (values, { props, setSubmitting }) => {
        setTimeout(() => {
            // console.log(JSON.stringify(values, null, 2));
            props.dispatch(userRegister(values));
            setSubmitting(false);
        }, 1000);
    },

    displayName: "RegisterForm"
})(RegisterForm);

const Register = connect(mapStateToProps)(RegisterModal);

function mapStateToProps(state) {
    return {
        user: state.user
    };
}

export default connect(mapStateToProps)(Register_Modal);
