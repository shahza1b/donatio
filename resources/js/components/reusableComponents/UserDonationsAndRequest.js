import React from "react";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import {
    faShare,
    faHandPointUp,
    faHeart
} from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import LoadingAnimation from "./LoadingAnimation";
import ProductDetailGallery from "./ProductDetailGallery";
import { Link } from "react-router-dom";

class UserDonationsAndRequest extends React.Component {
    state = {
        posts: {
            data: [],
            images: null
        },
        noPost: true,
        isLoading: true
    };
    componentDidMount() {
        let data = {
            user_id: this.props.user.credentials.id
        };
        axios
            .post("api/getUserPosts", data)
            .then(response => {
                if (response.data.data.length > 0) {
                    let Data = response.data.data;
                    let imagesName = Data.map(i => i.images.split(";"));
                    let newNames = [];
                    for (let i = 0; i < imagesName.length; i++) {
                        newNames.push([]);
                        imagesName[i].map(data =>
                            newNames[i].push({
                                original:
                                    location.origin.toString() +
                                    "/storage/" +
                                    data,
                                thumbnail:
                                    location.origin.toString() +
                                    "/storage/" +
                                    data
                            })
                        );
                    }
                    this.setState({
                        posts: {
                            data: Data,
                            images: newNames
                        },
                        noPost: false
                    });
                } else {
                    this.setState({
                        noPost: true
                    });
                }
                setTimeout(() => this.setState({ isLoading: false }), 2000);
                return response;
            });
    }
    render() {
        // console.log(this.state);
        return (
                <div className="container">
                    {this.state.isLoading ? (
                        <LoadingAnimation />
                    ) : (
                        <div className="row">
                            <div
                                className="col-md-12"
                                style={{
                                    padding: "15px 0 20px 0"
                                }}
                            >
                                <Tabs
                                    defaultActiveKey={this.state.activeTab}
                                    // {this.props.activeTab ? this.props.activeTab : 'my-donations'}
                                    id="donation-tabs"
                                >
                                    <Tab
                                        eventKey="my-donations"
                                        title="My Donations"
                                    >
                                        {!this.state.noPost ? (
                                            this.state.posts.data.map(
                                                (data, index) => (
                                                    <div
                                                        key={data.id}
                                                        className="container"
                                                    >
                                                        <div className="row mt-4">
                                                            <div className="mydonations col-md-2 height-100%">
                                                                <ProductDetailGallery
                                                                    key={index}
                                                                    data={
                                                                        this
                                                                            .state
                                                                            .posts
                                                                            .images[
                                                                            index
                                                                        ]
                                                                    }
                                                                    noThumbnail="true"
                                                                />
                                                            </div>
                                                            <div className="col-md-10">
                                                                <div className="row">
                                                                    <h3 className="col-md-6 mb-2">
                                                                        {
                                                                            data.title
                                                                        }
                                                                    </h3>
                                                                    <div className="col-md-6">
                                                                        <p className="float-right">
                                                                            Status:
                                                                            <span
                                                                                style={{
                                                                                    color:
                                                                                        "orange",
                                                                                    paddingLeft:
                                                                                        "3px",
                                                                                    fontWeight:
                                                                                        "bold"
                                                                                }}
                                                                            >
                                                                                {
                                                                                    data.status
                                                                                }
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                </div>

                                                                <div className="row pt-1">
                                                                    <div className="col-md-6 ">
                                                                        <span className="text-muted">
                                                                            <i
                                                                                className="fa fa-map-marker"
                                                                                aria-hidden="true"
                                                                            />{" "}
                                                                            &nbsp;{" "}
                                                                            {data.city +
                                                                                " " +
                                                                                data.country}{" "}
                                                                            &nbsp;
                                                                        </span>
                                                                        <span>
                                                                            <i
                                                                                className="fa fa-clock-o"
                                                                                aria-hidden="true"
                                                                            />
                                                                            &nbsp;{" "}
                                                                            {
                                                                                data.created_at
                                                                            }
                                                                        </span>
                                                                    </div>
                                                                    <div className="col-md-6">
                                                                        <p className=" float-right">
                                                                            Approx-Value:
                                                                            <span
                                                                                style={{
                                                                                    color:
                                                                                        "#ef004c",
                                                                                    paddingLeft:
                                                                                        "3px",
                                                                                    fontWeight:
                                                                                        "bold"
                                                                                }}
                                                                            >
                                                                                {
                                                                                    data.approx_value
                                                                                }
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                </div>

                                                                <div className="row pt-1">
                                                                    <div className="col-md-6">
                                                                        <div className="row">
                                                                            <div className="col-md-2 d-inline-flex p-t-16">
                                                                                <FontAwesomeIcon
                                                                                    style={{
                                                                                        color:
                                                                                            "rgb(239, 0, 76)",
                                                                                        fontSize:
                                                                                            "1.4em"
                                                                                    }}
                                                                                    icon={
                                                                                        faHeart
                                                                                    }
                                                                                />
                                                                                <span className=" p-l-17">
                                                                                    {
                                                                                        data.hearts
                                                                                    }
                                                                                </span>
                                                                                {/* color="rgb(239, 0, 76)" */}
                                                                            </div>
                                                                            <div className="col-md-2 d-inline-flex p-t-16">
                                                                                <FontAwesomeIcon
                                                                                    style={{
                                                                                        color:
                                                                                            "#11c0c3",
                                                                                        fontSize:
                                                                                            "1.4em"
                                                                                    }}
                                                                                    icon={
                                                                                        faShare
                                                                                    }
                                                                                />
                                                                                <span className="p-l-17">
                                                                                    {
                                                                                        data.shares
                                                                                    }
                                                                                </span>
                                                                            </div>
                                                                            <div className="col-md-2 d-inline-flex p-t-16">
                                                                                <FontAwesomeIcon
                                                                                    style={{
                                                                                        color:
                                                                                            "#3eb53f",
                                                                                        fontSize:
                                                                                            "1.4em"
                                                                                    }}
                                                                                    icon={
                                                                                        faHandPointUp
                                                                                    }
                                                                                />
                                                                                <span className=" p-l-17">
                                                                                    {
                                                                                        data.views
                                                                                    }
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-md-6  p-t-16">
                                                                        <p className="float-right">
                                                                            {" "}
                                                                            Condition:
                                                                            <span
                                                                                style={{
                                                                                    color:
                                                                                        "#ef004c",
                                                                                    paddingLeft:
                                                                                        "3px",
                                                                                    fontWeight:
                                                                                        "bold"
                                                                                }}
                                                                            >
                                                                                {
                                                                                    data.product_condition
                                                                                }
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div className="row pt-4">
                                                                    <div className="col-md-8">
                                                                        <p className=" mb-4 text-dark">
                                                                            {
                                                                                data.description.slice(0,70) + '...'
                                                                            }{" "}
                                                                            <Link to={`/details/${data.url}`}>
                                                                            <span
                                                                                style={{
                                                                                    color:
                                                                                        "#ef004c"
                                                                                }}
                                                                            >
                                                                                Read
                                                                                More
                                                                            </span>
                                                                            </Link>
                                                                            {" "}
                                                                        </p>
                                                                    </div>
                                                                    {/* <div className="col-md-4">
                                                                        <button
                                                                            style={{
                                                                                position:
                                                                                    "absolute",
                                                                                bottom:
                                                                                    "18px",
                                                                                right:
                                                                                    "0",
                                                                                padding:
                                                                                    "10px 25px",
                                                                                background:
                                                                                    "#41b541",
                                                                                color:
                                                                                    "white",
                                                                                borderRadius:
                                                                                    "25px"
                                                                            }}
                                                                        >
                                                                            REQUEST
                                                                            NOW
                                                                        </button>
                                                                    </div> */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr />
                                                    </div>
                                                )
                                            )
                                        ) : (
                                            <div style={{ height: "30vh" }}>
                                                <div
                                                    className="alert alert-danger alert-dismissible fade show ml-3 mt-2"
                                                    role="alert"
                                                >
                                                    No Donations Found!
                                                    <button
                                                        type="button"
                                                        className="close"
                                                        data-dismiss="alert"
                                                        aria-label="Close"
                                                    >
                                                        <span aria-hidden="true">
                                                            &times;
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        )}
                                    </Tab>
                                    <Tab
                                        eventKey="my-requests"
                                        title="My Request"
                                    ></Tab>
                                </Tabs>
                                {/* padding: 15px 0 20px 0; */}
                            </div>
                        </div>
                    )}
                </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    };
}

export default connect(mapStateToProps)(UserDonationsAndRequest);
