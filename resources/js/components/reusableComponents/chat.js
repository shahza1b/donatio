import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import RatingStars from "./RatingStars";

const Chat = () => {
    const data = [
        {
            id: "1",
            image: "../images/img.jpg",
            name: "Author Name",
            location: "Gujranwala",
            rating: "5"
        },
        {
            id: "2",
            image: "../images/img.jpg",
            name: "Author Name",
            location: "Gujranwala",
            rating: "5"
        },
        {
            id: "3",
            image: "../images/img.jpg",
            name: "Author Name",
            location: "Gujranwala",
            rating: "5"
        },
        {
            id: "4",
            image: "../images/img.jpg",
            name: "Author Name",
            location: "Gujranwala",
            rating: "5"
        },
        {
            id: "5",
            image: "../images/img.jpg",
            name: "Author Name",
            location: "Gujranwala",
            rating: "5"
        },
        {
            id: "6",
            image: "../images/img.jpg",
            name: "Author Name",
            location: "Gujranwala",
            rating: "5"
        }
    ];
    return (
        <div>
            {/* <Header /> */}
            <div className="container">
                <div className="row mt-5">
                    <div className="col-md-12 col-md-offset-0">
                        <div className="irs-section-title">
                            <h2 style={{ color: "#4848484" }}>MESSAGES</h2>
                            <div className="irs-title-line">
                                <div
                                    className="irs-title-icon "
                                    style={{ left: "41%" }}
                                >
                                    <img src="https://donatio.love/resources/assets/public/images/icon.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div
                    style={{
                        border: "1px solid #8080804f",
                        borderRadius: "15px",
                        backgroundColor: "white",
                        marginBottom: "4rem"
                    }}
                >
                    <div className="row">
                        <div className="col-md-4 border-right">
                            <div className="wrap-input1000 m-b-16 validate-input">
                                <input
                                    required
                                    className="input100"
                                    type="text"
                                    name="search"
                                    placeholder="Search"
                                    style={{
                                        border: "1px solid #8080804a",
                                        margin: "20px 40px 32px 34px"
                                    }}
                                />
                            </div>
                            <div className="tiles">
                                {data.map(data => (
                                    <div key={data.id}>
                                        <div className="row mt-3 mb-3">
                                            <div className="col-md-7 m-l-16">
                                                <img
                                                    src={data.image}
                                                    className="rounded-circle mx-auto d-block float-left m-r-12"
                                                    alt="Profile Image"
                                                    style={{
                                                        width: "55px",
                                                        height: "50px"
                                                    }}
                                                />
                                                <h5
                                                    style={{
                                                        color: "black",
                                                        margin:
                                                            "6px 0px 0px 70px",
                                                        fontSize: "16px"
                                                    }}
                                                >
                                                    {data.name}
                                                </h5>
                                                <p
                                                    style={{
                                                        paddingBottom: ".5em",
                                                        marginLeft: "70px",
                                                        fontSize: "13px"
                                                    }}
                                                >
                                                    {data.location}
                                                </p>
                                            </div>
                                            <div className="col-md-4 mt-2">
                                                <div className="float-right">
                                                    <RatingStars
                                                        style={{
                                                            fontSize: "50px"
                                                        }}
                                                        editing="true"
                                                        color="#ef004c"
                                                        name="chat"
                                                        rating={data.rating}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <hr style={{ width: "363px" }} />
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="col-md-8 ">
                            <div className="row mt-2 mb-2">
                                <div className="col-md-6">
                                    {" "}
                                    <img
                                        src="../images/img.jpg"
                                        style={{
                                            height: "75px",
                                            width: "70px",
                                            borderRadius: "15px",
                                            float: "left"
                                        }}
                                    />
                                    <div className="d-inline-block p-l-24">
                                        <h5 className="m-b-10 m-t-13">
                                            Dangerous Black T-Shirt
                                        </h5>
                                        <p>Gujranwala,Pakistan</p>
                                    </div>
                                </div>
                                <div className="col-md-6 ">
                                    <div className="float-right">
                                        <button
                                            className=""
                                            style={{
                                                color: "#ef004c",
                                                border: "1px solid #ef004c",
                                                borderRadius: "25px",
                                                padding: "0px 24px",
                                                height: "48px",
                                                margin: "15px 6px"
                                            }}
                                        >
                                            DECLINE
                                        </button>
                                        <button
                                            style={{
                                                padding: "0px 24px",
                                                background: "#41b541",
                                                color: "white",
                                                borderRadius: "25px",
                                                height: "48px",
                                                margin: "15px 6px"
                                            }}
                                        >
                                            APPROVE
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div>
                                <div
                                    className="message-area d-flex justify-content-end"
                                    style={{
                                        height: "485px",
                                        flexDirection: "column"
                                    }}
                                >
                                    <div style={{ width: "100%" }}>
                                        <div
                                            className="receive-message"
                                            style={{
                                                background: "#8080802b",
                                                maxWidth: "330px",
                                                height: "auto",
                                                borderRadius: "25px",
                                                margin: "26px 34px"
                                            }}
                                        >
                                            <h6 style={{ padding: "7px 26px" }}>
                                                I want this donation as i'm
                                                deserving person.
                                            </h6>
                                        </div>
                                    </div>
                                    <div style={{ width: "100%" }}>
                                        <div
                                            className="send-message d-flex justify-content-end"
                                            style={{
                                                background: "#ef004c",
                                                maxWidth: "330px",
                                                height: "auto",
                                                borderRadius: "25px",
                                                margin: "26px 34px",
                                                float: "right"
                                            }}
                                        >
                                            <h6
                                                style={{
                                                    padding: "7px 26px",
                                                    color: "white "
                                                }}
                                            >
                                                Let me ponder over it for a
                                                while. I will lwt you know
                                            </h6>
                                        </div>
                                    </div>
                                    <div
                                        style={{
                                            width: "100%",
                                            display: "flex",
                                            marginBottom: "20px"
                                        }}
                                    >
                                        <input
                                            required
                                            className="input100"
                                            type="text"
                                            name="type a message"
                                            placeholder="Type a message"
                                            style={{
                                                border: "1px solid gray",
                                                width: "90%",
                                                marginTop: "8px",
                                                marginRight: "12px",
                                                height: "50px !important"
                                            }}
                                        />
                                        <img src="./images/send-button.png" style={{
                                            marginRight: '15px !important'
                                         }}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <Footer /> */}
        </div>
    );
};

export default Chat;
