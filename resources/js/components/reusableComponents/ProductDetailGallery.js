import React from "react";
import ImageGallery from "react-image-gallery";

const PREFIX_URL =
    "https://raw.githubusercontent.com/xiaolin/react-image-gallery/master/static/";

class ProductDetailGallery extends React.Component {
    constructor() {
        super();
        this.state = {
            autoPlay: false,
            showIndex: false,
            showBullets: true,
            infinite: true,
            showThumbnails: true,
            showFullscreenButton: false,
            showGalleryFullscreenButton: true,
            showPlayButton: true,
            showGalleryPlayButton: true,
            showNav: false,
            isRTL: false,
            slideDuration: 450,
            slideInterval: 2000,
            slideOnThumbnailOver: false,
            thumbnailPosition: "bottom"
        };

        this.images = [
            {
                original: `${PREFIX_URL}1.jpg`,
                thumbnail: `${PREFIX_URL}1t.jpg`,
                originalClass: "featured-slide",
                thumbnailClass: "featured-thumb",
                description: "Custom class for slides & thumbnails"
            }
        ]
    }

    componentDidMount() {
        if ((this.props.noThumbnail === "true") || (this.props.data.length === 1)) {
            this.setState({
                showThumbnails: false,
                showFullscreenButton: false,
                showGalleryFullscreenButton: false,
                showPlayButton: false
            });
        }

        if(this.props.data.length === 1) {
            this.setState({
                showBullets: false
            })
        }
    }

    render() {
        return (
            <section className="app">
                <ImageGallery
                    autoPlay={this.state.autoPlay}
                    ref={(i) => (this._imageGallery = i)}
                    items={this.props.data ? this.props.data : this.images}
                    lazyLoad={true}
                    infinite={this.state.infinite}
                    showBullets={this.state.showBullets}
                    showFullscreenButton={
                        this.state.showFullscreenButton &&
                        this.state.showGalleryFullscreenButton
                    }
                    showPlayButton={
                        this.state.showPlayButton &&
                        this.state.showGalleryPlayButton
                    }
                    showThumbnails={this.state.showThumbnails}
                    showIndex={this.state.showIndex}
                    showNav={this.state.showNav}
                    isRTL={this.state.isRTL}
                    thumbnailPosition={this.state.thumbnailPosition}
                    slideDuration={parseInt(this.state.slideDuration)}
                    slideInterval={parseInt(this.state.slideInterval)}
                    slideOnThumbnailOver={this.state.slideOnThumbnailOver}
                    additionalClass="app-image-gallery"
                />
            </section>
        );
    }
}

export default ProductDetailGallery;
