import React from "react";
import RatingStars from "./RatingStars";
import { Link } from "react-router-dom";

function scrollToTop() {
    window.scroll({
        top: 100,
        left: 0,
        behavior: "smooth"
    });
}

function DonorProfileCard({name, location, rating, profileImage, username}) {
    return (
        <div className="col-lg-3 col-md-4 mt-4">
            <div
                className="profile-card"
                style={{
                    height: "300px"
                }}>
                <Link to={`/profile/${username}`} onClick={scrollToTop}>
                    <img
                        src={profileImage ? profileImage : "/images/donors_icon.png"}
                        className="rounded mx-auto d-block"
                        alt="Profile Image"
                        height="170px"
                    />
                    <h4> {name ? name : "Auther Name"} </h4>
                    <RatingStars sta={rating ? rating : 3}/>
                    <i className="fa fa-map-marker" />
                    &nbsp;&nbsp;{location ? location : "Gujranwala, Pakistan"}
                </Link>
            </div>
        </div>
    );
}

export default DonorProfileCard;
