import React from "react";

function handleChange(event) {
    var files = event.target.files; //FileList object
    var output = document.getElementById("result");

    for (var i = 0; i < 10; i++) {
        var file = files[i];
        var picReader;
        picReader = new FileReader();

        //Only pics
        if (!file.type.match("image")) continue;

        picReader.addEventListener("load", function(event) {
            var picFile = event.target;

            var div = document.createElement("div");
            div.setAttribute("class", "col-md-3");

            div.innerHTML =
                "<img class='img-thumbnail' style='height: 110px; width: 100%' src='" +
                picFile.result +
                "'" +
                "title='" +
                picFile.name +
                "'/>";

            output.insertBefore(div, null);
        });

        //Read the image
        picReader.readAsDataURL(file);
    }
}

export default class UploadImg extends React.Component {
    render() {
        return (
            <div className="row" id="result">
                <div className="col-md-10">
                    <p className="p-l-10 text-dark ">
                        Upload Images{" "}
                        <span style={{ color: "#ef004c", fontSize: "13px" }}>
                            (Max 10 images can be attached)
                        </span>{" "}
                    </p>
                    <input
                        style={{
                            width: "100%",
                            border: "none",
                            padding: "11px"
                        }}
                        id="files"
                        type="file"
                        multiple
                        onChange={handleChange}
                    />
                </div>
            </div>
        );
    }
}
