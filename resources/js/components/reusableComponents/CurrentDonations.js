import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const CurrentDonations = ({
    title,
    description,
    url,
    images,
    button,
    username
    }) => {
    const user = useSelector(state => state.user.credentials);
    
    return (
        <>
            <div className="row mt-4">
                <div className="col-md-3 height-100%">
                    <img
                        src={images}
                        style={{
                            height: "164px",
                            width: "100%",
                            borderRadius: "15px"
                        }}
                    />
                </div>
                <div className="col-md-9">
                    <h3 className="mb-2">{title}</h3>
                    <p className="mb-4 text-dark" style={{ height: "4em" }}>
                        {description && description.slice(0, 200) + "...  "}{" "}
                        <Link to={`/details/${url}`}>
                            <span style={{ color: "#ef004c" }}>Read More</span>{" "}
                        </Link>
                    </p>
                    {
                        username &&
                        username !== user.username &&
                        (button === "current" ? (
                            <button
                                style={{
                                    padding: "10px 25px",
                                    background: "#41b541",
                                    color: "white",
                                    borderRadius: "25px"
                                }}
                            >
                                REQUEST NOW
                            </button>
                        ) : (
                            <button
                                style={{
                                    padding: "10px 25px",
                                    background: "#ef004c",
                                    color: "white",
                                    borderRadius: "25px"
                                }}
                            >
                                DONATED
                            </button>
                        ))}
                </div>
            </div>
            <hr style={{ marginTop: "1.3rem" }} />
        </>
    );
};

export default CurrentDonations;
