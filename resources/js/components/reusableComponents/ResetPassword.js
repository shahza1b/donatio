import React, { useEffect } from "react";
import { useSelector, connect } from 'react-redux'
import Modal from "react-bootstrap/Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { withFormik } from "formik";
import { changePassword } from "../Store/Actions";
import Spinner from "react-spinner-material";
import { SET_LOADING } from "../Store/consts";
// import Slide from "react-reveal/Slide";


const ResetPassword = props => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        setFieldValue,
        isSubmitting
    } = props;
    const [show, setShow] = React.useState(false);
    const Errors = useSelector(state => state.UI.errors)
    const loading = useSelector(state => state.UI.loading)

    
    return (
        <div>
             <button
                className="dropdown-item"
                type="button"
                onClick={() => setShow(true)}
                >
                    <i
                        style={{
                            width:
                                "2.8em"
                        }}
                        className="fa fa-key"
                        aria-hidden="true"></i>
                    <span>
                        Change Password
                    </span>
            </button>
            <Modal
                show={show}
                onHide={() => setShow(false)}
                backdrop={true}
                dialogClassName="modal-10w"
                aria-labelledby="example-custom-modal-styling-title"
            >
                <Modal.Header closeButton>
                    <button
                        onClick={() => setShow(false)}
                        className="p-t-5 p-r-15"
                    >
                        <FontAwesomeIcon icon={faChevronLeft} />
                    </button>
                    <Modal.Title id="forgot-password-modal-title">
                        Change Password
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form
                        id="loginForm"
                        className="login100-form validate-form p-l-15 p-r-15"
                        onSubmit={handleSubmit}
                    >
                        <div className="row row-centered">
                            <div
                                className="col"
                                style={{ paddingBottom: "1em" }}
                            >
                                <br />
                                Please enter your old password and then new password.
                            </div>
                        </div>
                        <div
                            className="wrap-input100 validate-input m-b-16"
                            data-validate="Please enter username"
                        >
                            <input
                                required
                                className="input100"
                                type="password"
                                name="oldPassword"
                                placeholder="Old Password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.oldPassword}
                                minLength={6}
                            />
                            <span className="focus-input100" />
                        </div>
                        <div
                            className="wrap-input100 validate-input m-b-16"
                            data-validate="Please enter username"
                        >
                            <input
                                required
                                className="input100"
                                type="password"
                                name="newPassword"
                                placeholder="New Password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.newPassword}
                                minLength={6}
                            />
                            <span className="focus-input100" />
                        </div>
                        <div
                            className="wrap-input100 validate-input m-b-16"
                            data-validate="Please enter username"
                        >
                            <input
                                required
                                className="input100"
                                type="password"
                                name="confirmNewPassword"
                                placeholder="Confirm New Password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.confirmNewPassword}
                                minLength={6}
                            />
                            <span className="focus-input100" />
                        </div>
                       {(touched.newPassword)
                        && (touched.confirmNewPassword) 
                        && (values.newPassword !== values.confirmNewPassword) 
                        &&  <div className="alert alert-warning" role="alert">
                            Password Doesn't match
                            </div>}
                        {
                            Errors && 
                            <div className="alert alert-info" role="alert">
                                {Errors.password}
                            </div>
                        }
                        <div className="container-login100-form-btn">
                            <button
                                className="login100-form-btn"
                                type="submit"
                                style={{ width: "100%" }}
                                disabled={loading}
                            >
                                {loading ? <Spinner
                                                size={20}
                                                spinnerColor={"#fff"}
                                                spinnerWidth={2}
                                                visible={true}
                                                /> : "SUBMIT"
                                        }
                            </button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        </div>
    );
};



export default connect()(withFormik({
    mapPropsToValues: () => ({
        oldPassword: "",
        newPassword: "",
        confirmNewPassword: "",
    }),

    handleSubmit: (values, { props, setSubmitting }) => {
        props.dispatch({ type: SET_LOADING })
        if(values.newPassword !== values.confirmNewPassword) return
        setTimeout(async () => {
            await props.dispatch(changePassword(values))
            setTimeout(() => setSubmitting(false), 3500);
        }, 1000);
    },
})(ResetPassword));

