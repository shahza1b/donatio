import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Footer extends Component {
    render() {
        return (
            <div>
                <section className="irs-copyright-field" style={styles}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="footer_navs">
                                    <ul>
                                        <li>
                                            <Link to="/">HOME</Link>
                                        </li>
                                        <li>
                                            <Link to="/about">ABOUT</Link>
                                        </li>
                                        <li>
                                            <Link to="/policy">
                                                PRIVACY POLICY
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/contact-us">
                                                CONTACT
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                                <div className="irs-copyright">
                                    <p
                                        style={{
                                            color: "black",
                                            paddingTop: "15"
                                        }}
                                    >
                                        Copyrights © {new Date().getFullYear()}. All rights reserved
                                        by{" "}
                                        <a
                                            href="https://gresys.com/"
                                            target="_blank"
                                            style={{ fontSize: "15" }}
                                        >
                                            Green Systems
                                        </a>{" "}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const styles = {
    backgroundImage: `url("../images/footer_img.png")`,
    backgroundSize: "cover",
    backgroundRepeat: "noRepeat",
    height: "42vh",
    paddingTop: "65px"
};
