import React, { useEffect, useState } from "react";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import LatestDonationComponent from "../reusableComponents/latestDonationComponent";
import GoogleApiWrapper from "../reusableComponents/GoogleMap";
import { withFormik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { setCities } from "../Store/Actions/countryActions";
import { setLatestPosts } from './../Store/Actions/postActions';

const DonationContent = props => {
    const [city, setCity] = useState("");
    const cities = useSelector(state => state.cities);
    const latestPosts = useSelector(state => state.latestPosts);
    const { data } = latestPosts;

    
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(setCities(city));
    }, [city]);
    useEffect(() => {
        dispatch( setLatestPosts() )
    }, [])
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit
    } = props;
    return (
        <section className="irs-campaign-field">
            <div className="container">
                <form onSubmit={handleSubmit}>
                    <div className="row">
                        <div className="form-group col-md-3">
                            <input
                                type="text"
                                style={{
                                    borderRadius: "20px",
                                    padding: "20px 18px"
                                }}
                                className=" input100"
                                name="city"
                                id="city"
                                placeholder="City"
                                onChange={e => setCity(e.target.value)}
                                onBlur={handleBlur}
                                value={city}
                                list="cities"
                            />
                        </div>
                        <div className="form-group col-md-4">
                            <select
                                style={{
                                    borderRadius: "20px"
                                }}
                                className=" input100"
                                id="category_id"
                                name="category"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.category}
                            >
                                <option defaultValue value="1">
                                    Category
                                </option>
                                <option value="food">Food</option>
                                <option value="clothes">Clothes</option>
                                <option value="shoes">Shoes</option>
                                <option value="books">Books</option>
                                <option value="blood">Blood</option>
                                <option value="body-organs">Body Organs</option>
                                <option value="bags">Bags</option>
                                <option value="sun-glasses">Sun Glasses</option>
                                <option value="household_stuff">
                                    Household Stuff
                                </option>
                                <option value="mobile_accessories">
                                    Mobile Accessories
                                </option>
                                <option value="computer_accessories">
                                    Computer Accessories
                                </option>
                                <option value="electronics">Electronics</option>
                                <option value="furniture_items">
                                    Furniture/Wood Items
                                </option>
                                <option value="others">Others</option>
                            </select>
                        </div>
                        <div className="form-group col-md-3">
                            <input
                                style={{
                                    borderRadius: "20px",
                                    padding: "20px 18px"
                                }}
                                type="text"
                                className=" input100"
                                name="keyword"
                                placeholder="Keyword"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.keyword}
                            />
                        </div>
                        <div className="col-md-2">
                            <button
                                type="submit"
                                style={{
                                    borderRadius: "20px",
                                    borderColor: "#ef004c",
                                    padding: "11px 40px",
                                    backgroundColor: "#ef004c",
                                    width: "100%",
                                    height: "75%"
                                }}
                                className="btn btn-primary center-block"
                            >
                                <i className="fa fa-search" /> Search
                            </button>
                        </div>
                    </div>
                </form>
                <br />
                <div className="row">
                    <div className="col-md-12" style={{ height: "50vh" }}>
                        <GoogleApiWrapper />
                    </div>
                    <div
                        className="col-md-12"
                        style={{
                            padding: "15px 0 20px 0"
                        }}
                    >
                        <Tabs
                            defaultActiveKey="available_donations"
                            id="donation-tabs"
                        >
                            <Tab
                                eventKey="available_donations"
                                title="Available Donations"
                            >
                                {latestPosts &&
                                    data.donations.map((post) => (
                                        <LatestDonationComponent
                                        key={post.id}
                                        image={post.images}
                                        date={post.created_at}
                                        location={post.city + " " + post.country}
                                        description={post.description.slice(0, 74) + "..."}
                                        price={post.approx_value}
                                        requests={post.request ? post.requests : null}
                                        authorName={post.user.name}
                                        authorImage={post.user.profileImage}
                                        username={post.user.username}
                                        rating={post.rating ? post.rating : "0"}
                                        url={post.url}
                                    />
                                    ))}
                            </Tab>
                            <Tab eventKey="donated_items" title="Donated Items">
                            {latestPosts &&
                                    data.donated.map((post) => (
                                        <LatestDonationComponent
                                        key={post.id}
                                        image={post.images}
                                        date={post.created_at}
                                        location={post.city + " " + post.country}
                                        description={post.description.slice(0, 74) + "..."}
                                        price={post.approx_value}
                                        requests={post.request ? post.requests : null}
                                        authorName={post.user.name}
                                        authorImage={post.user.profileImage}
                                        username={post.user.username}
                                        rating={post.rating ? post.rating : "0"}
                                        url={post.url}
                                    />
                                    ))}
                            </Tab>
                        </Tabs>
                        {/* padding: 15px 0 20px 0; */}
                    </div>
                </div>
            </div>
            <datalist id="cities">
                {cities.data &&
                    cities.data.map(city => (
                        <option key={city.id} value={`${city.name}`} />
                    ))}
            </datalist>
        </section>
    );
};
export default withFormik({
    mapPropsToValues: () => ({
        city: "",
        category: "",
        keyword: ""
    }),

    handleSubmit: (values, { setSubmitting }) => {
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: "BasicForm"
})(DonationContent);
