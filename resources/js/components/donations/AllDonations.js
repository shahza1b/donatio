import React from "react";
import Header from "../reusableComponents/Header";
import Footer from "../reusableComponents/Footer";
import DonationContent from "./DonationContent";

const AllDonations = () => {
    return (
        <div>
            {/* <Header /> */}
            <DonationContent />
            {/* <Footer /> */}
        </div>
    );
};

export default AllDonations;
