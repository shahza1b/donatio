import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect, useSelector } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Homepage from "./homePage/index";
import AllDonations from "./donations/AllDonations";
import AllDonors from "./donors/AllDonors";
import About from "./about/About";
import PrivacyPolicy from "./privacyPolicy/index";
import CreateDonation from "./createDonation/createDonation";
import ContactUs from "./contactUs/ContactUs";
import Profile from "./reusableComponents/profile";
import DonatedItemDetail from "./reusableComponents/donatedItemDetail";
import Dashboard from "./dashBoard/index";
import UserDonationsAndRequest from "./reusableComponents/UserDonationsAndRequest";
import EditProfile from "./reusableComponents/editProfile";
import Chat from "./reusableComponents/chat";
import Header from "./reusableComponents/Header";
import Footer from "./reusableComponents/Footer";

import _404 from "./404/index";
// import swal from "sweetalert";
// import ChangePassword from "./reusableComponents/ChangePassword";
import { userLogout } from './Store/Actions/index';

const PrivateRoute = ({ component: Component, ...rest }) => {
    const isLoggedIn = useSelector(state => state.user.isLoggedIn);
    return (
        <Route
            {...rest}
            render={props => {
                if (isLoggedIn) {
                    return <Component {...props} />;
                } else {
                    toast.error("You need to be Log In to view this page");
                    return <Redirect to="/" />;
                }
            }}
        />
    );
};

class Routes extends React.Component {
    componentDidMount() {
        const isLoggedIn = this.props.user.isLoggedIn
        if (!isLoggedIn) {
            localStorage.removeItem('DonatioIdToken')
        } else if(!localStorage.DonatioIdToken) {
            window.history.pushState(null, null, '/')
            setTimeout(() => this.props.userLogout(), 10)
        }
    }
    render() {
        return (
            <div>
                <Header />
                <Switch>
                    <Route exact path="/" component={Homepage} />
                    <Route exact path="/alldonations" component={AllDonations} />
                    <Route exact path="/donors" component={AllDonors} />
                    <Route exact path="/about" component={About} />
                    <Route exact path="/policy" component={PrivacyPolicy} />
                    <PrivateRoute
                        path="/createdonation"
                        component={CreateDonation}
                    />
                    <Route exact path="/contact-us" component={ContactUs} />
                    <Route exact path="/details/:username/:title" component={DonatedItemDetail} />
                    <Route exact path="/profile/:username" component={Profile} />
                    <PrivateRoute path="/dashboard" component={Dashboard} />
                    <PrivateRoute
                        path="/my-donations"
                        component={UserDonationsAndRequest}
                    />
                    <PrivateRoute
                        path="/my-requests"
                        component={UserDonationsAndRequest}
                    />
                    <PrivateRoute path="/editprofile" component={EditProfile} />
                    <PrivateRoute path="/inbox" component={Chat} />
                    {/* <PrivateRoute path="/change-password" component={ChangePassword} /> */}
                    <Route render={() => <_404 title="Page" />} />
                </Switch>
                <Footer />
                <ToastContainer
                    position="bottom-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.user
    };
}

const mapDispatchToProps = {
    userLogout
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
