import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

// Routes
import Routes from "./Routes";

// Store
import { persistor, Store } from './Store'

// Redux Provider for React
import { Provider } from "react-redux";

// Redux Persist Gate for React
import { PersistGate } from "redux-persist/lib/integration/react";

// Actions
import { setDonors } from "./Store/Actions/donorsActions";
import { setLatestPosts } from "./Store/Actions/postActions";
// import JwtDecode from "jwt-decode";



export default class App extends Component {

    componentDidMount() {
        // Dispatch Actions
        Store.dispatch(setDonors());
        Store.dispatch(setLatestPosts());
    }
    render() {
        return (
            <Provider store={Store}>
                <PersistGate persistor={persistor}>
                    <BrowserRouter>
                        <Routes />
                    </BrowserRouter>
                </PersistGate>
            </Provider>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("app"));
