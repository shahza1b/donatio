import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import RecieversIcon from "./imgs/receivers_icon.png";
import DonorsIcon from "./imgs/donors_icon.png";
import DonationsIcon from "./imgs/donations_icon.png";
import LatestDonationComponent from "../reusableComponents/latestDonationComponent";
import DonorProfileCard from "../reusableComponents/DonorProfileCard";
import { Link } from "react-router-dom";
import RegisterModal from "./../reusableComponents/registerModal";
import { connect } from "react-redux";
import { setLatestPosts } from "../../components/Store/Actions/postActions";

class IndexContent extends Component {
    scrollToTop() {
        window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth"
        });
    }
    scrollToCreateDonations() {
        window.scroll({
            top: 100,
            left: 0,
            behavior: "smooth"
        });
    }

    componentDidMount() {
        this.props.setLatestPosts()
    }
    render() {
        const {
            donors,
            recieverCount,
            latestPosts: { data, count }
        } = this.props;
        return (
            <div>
                {/* campaign start */}
                <section className="irs-campaign-field">
                    <div className="container">
                        <div className="row align-item-center justify-content-center">
                            <div className="col-md-6 col-md-offset-3">
                                <div className="irs-section-title">
                                    <h2>LATEST DONATIONS</h2>
                                    <div className="irs-title-line">
                                        <div
                                            className="irs-title-icon"
                                            style={{ left: "41%" }}
                                        >
                                            <img src="https://donatio.love/resources/assets/public/images/icon.png" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row row-centered">
                            {data.donations.slice(0, 6).map((post) => (
                                <LatestDonationComponent
                                    key={post.id}
                                    image={post.images}
                                    date={post.created_at}
                                    location={post.city + " " + post.country}
                                    description={post.description.slice(0, 74) + "..."}
                                    price={post.approx_value}
                                    requests={post.request ? post.requests : null}
                                    authorName={post.user.name}
                                    authorImage={post.user.profileImage}
                                    username={post.user.username}
                                    rating={post.rating ? post.rating : "0"}
                                    url={post.url}
                                />
                            ))}
                        </div>
                        <div className="text-center">
                            <Link
                                className="btn btn-outline-danger"
                                to="/alldonations"
                                role="button"
                                style={{
                                    padding: "12px 18px",
                                    borderRadius: "25px",
                                    marginTop: "2em"
                                }}
                                onClick={this.scrollToTop}
                            >
                                VIEW ALL
                            </Link>
                        </div>
                    </div>
                </section>
                {/* campaign end */}
                <section
                    className="irs-counter-field parallax irs-layer-black"
                    data-stellar-background-ratio="0.3"
                    style={{ backgroundPosition: "50% -127.717px" }}
                >
                    <div className="container">
                        <div className="row text-center align-items-center justify-content-center vertical-align-center">
                            <div className="col-md-4 col-sm-4 col-xs-12">
                                <div className="row">
                                    <div className="col float-right p-r-0">
                                        <img src={RecieversIcon} width="75%" />
                                    </div>
                                    <div className="col float-left text-left p-l-0 p-t-30 font-weight-bold text-white">
                                        <p className="info-text">
                                            {recieverCount}
                                            <br />
                                            RECIEVERS
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-4 col-xs-12">
                                <div className="row">
                                    <div className="col float-right p-r-0">
                                        <img src={DonationsIcon} width="75%" />
                                    </div>
                                    <div className="col float-left text-left p-l-0 p-t-30 font-weight-bold text-white">
                                        <p className="info-text">
                                            {count}
                                            <br />
                                            DONATIONS
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-4 col-xs-12">
                                <div className="row">
                                    <div className="col float-right p-r-0">
                                        <img src={DonorsIcon} width="75%" />
                                    </div>
                                    <div className="col float-left text-left p-l-0 p-t-30 font-weight-bold text-white">
                                        <p className="info-text">
                                            {donors.count}
                                            <br />
                                            DONORS
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="irs-volunteers-field">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 col-md-offset-0">
                                <div className="irs-section-title">
                                    <h2>
                                        Great people who are donating with love
                                    </h2>
                                    <div className="irs-title-line">
                                        <div
                                            className="irs-title-icon "
                                            style={{ left: "41%" }}
                                        >
                                            <img src="https://donatio.love/resources/assets/public/images/icon.png" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row row-centered">
                            {donors.data &&
                                donors.data.slice(0, 12).map((donor, i) => (
                                    <DonorProfileCard
                                        key={i}
                                        name={donor.name}
                                        location={donor.location}
                                        rating={donor.rating}
                                        profileImage={donor.profileImage}
                                        username={donor.username}
                                    />
                                ))}
                        </div>
                        <div className="text-center p-t-40">
                            <Link
                                className="btn btn-outline-danger"
                                to="/donors"
                                role="button"
                                style={{
                                    padding: "12px 18px",
                                    borderRadius: "25px"
                                }}
                                onClick={this.scrollToTop}
                            >
                                VIEW ALL
                            </Link>
                        </div>
                    </div>
                </section>
                <section
                    className="irs-help-field"
                    style={{
                        background:
                            "rgba(85, 85, 85, 0.02) none repeat scroll 0% 0%",
                        borderTop: "1px solid #0000001a"
                    }}
                >
                    <div className="container">
                        <div className="row align-items-center justify-content-center">
                            <div className="col-md-6 col-md-offset-3">
                                <div className="irs-section-title">
                                    <h2>HOW IT WORKS</h2>
                                    <div className="irs-title-line">
                                        <div
                                            className="irs-title-icon"
                                            style={{ left: "41%" }}
                                        >
                                            <img src="https://donatio.love/resources/assets/public/images/icon.png" />
                                        </div>
                                    </div>
                                    <p>
                                        Donatio is donation-based platform that
                                        fits best for individuals! Enjoy and
                                        Spread the happiness by helping your
                                        fellow brothers For raising your fund
                                        Donatio set up a powerful fundraiser in
                                        few minutes{" "}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ padding: "0 15px" }}>
                            <div className="col-md-4 col-sm-4 irs-col-padd-less p-0">
                                <div className="irs-help-box bg1 align3">
                                    <img
                                        style={{ width: 60 }}
                                        src="../images/people_group.png"
                                    />
                                    <h4>Become A Donor</h4>
                                    <p>
                                        Do good have good…! If you believe &amp;
                                        want to make an impact in people's life
                                        then you are at right place.
                                        <RegisterModal title="Join now" /> as a
                                        donor.
                                        {/* style={{ textDecoration: "underline", color: "white" }} */}
                                    </p>
                                    {/* <a class="btn btn-default irs-btn-transparent" href="https://donatio.love/register" role="button"><i class="zmdi zmdi-arrow-right"></i></a> */}
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-4 irs-col-padd-less p-0">
                                <div className="irs-help-box bg2 irs-middle-box">
                                    {/* <i class="fa fa-file-text-o"></i> */}
                                    <img
                                        style={{ width: 60 }}
                                        src="https://donatio.love/resources/assets/public/images/donate.png"
                                    />
                                    <h4>Donate something </h4>
                                    <p>
                                        {" "}
                                        Donatio is providing a plateform where
                                        you can donate new and used items for
                                        needy people, to donate something click
                                        on below link.
                                    </p>
                                    <Link
                                        style={{
                                            border: `1px solid #fff`,
                                            padding: "8px 25px",
                                            borderRadius: "25px"
                                        }}
                                        className="btn btn-default irs-btn-transparent"
                                        to="/createdonation"
                                        role="button"
                                        onClick={this.scrollToCreateDonations}
                                    >
                                        <FontAwesomeIcon
                                            style={{ color: "#fff" }}
                                            icon={faArrowRight}
                                        />
                                    </Link>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-4 irs-col-padd-less p-0">
                                <div className="irs-help-box bg3 align3">
                                    {/* <i class="fa fa-user-circle-o "></i> */}
                                    <img
                                        style={{ width: 53 }}
                                        src="https://donatio.love/resources/assets/public/images/love.png"
                                    />
                                    <h4>Donations Spread Love</h4>
                                    <p>
                                        You have not lived until you have done
                                        something for someone who can not repay
                                        you. Your donations can make someone
                                        happy.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = ({ donors, latestPosts, reciever }) => ({
    recieverCount: reciever.count,
    donors: donors,
    latestPosts: latestPosts
});

export default connect(mapStateToProps, { setLatestPosts })(IndexContent);
