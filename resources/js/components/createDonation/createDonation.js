import React from "react";
import "./style.css";
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import { withFormik } from "formik";
import FileBase64 from "react-file-base64";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import axios from "axios";
import Spinner from "react-spinner-material";
import { withRouter } from "react-router";
import slugify from "slugify";
import { setCountries, setStates, setCities } from "../Store/Actions/countryActions";
import { setUserData } from '../Store/Actions'

class CreateDonation extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            images: [],
            error: "",
            submitting: false,
            country: '',
            state: '',
            city: '',
        };

        this.props.setFieldValue('username', this.props.user.username)
    }

    componentDidMount() {
        this.props.setUserData("Create Donation")
        this.props.setCountries()
        this.props.values.username = this.props.user.username

    }

    getFiles = (files) => {
        if (files.length > 10) {
            return toast.info("Max 10 images can be selected!", {
                position: "bottom-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
        const { values, user } = this.props;
        let base64Images = [];
        files.map((data, index) => (base64Images[index] = data.base64));

        values.images = base64Images;
        values.username = user.username
        this.setState({
            images: base64Images,
            error: ""
        });
    };

    handleDelete = (index) => {
        const items = this.state.images.filter((item) => item !== index);
        const { values } = this.props;
        values.images = items;
        this.setState({
            images: items,
            error: ""
        });
    };

    handleCountryChange = (e) => {
        this.setState({country: e.target.value}, () => {
            this.props.setCountries(this.state.country)
            this.props.setFieldValue('country', this.state.country.split("(+")[0])
        })
    }    
    
    handleStateChange = (e) => {
        this.setState({state: e.target.value}, () => {
            this.props.setStates(this.state.state)
            this.props.setFieldValue('state', this.state.state)
        })
    }

    handleCityChange = (e) => {
        this.setState({city: e.target.value}, () => {
            this.props.setCities(this.state.city)
            this.props.setFieldValue('city', this.state.city)
        })
    }


    render() {
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
            handleSubmit,
            user,
            handleReset,
            isSubmitting,
            countries,
            states,
            cities
        } = this.props;
        return (
            <div className="bg-color">
                {/* <Header /> */}
                <div className="container">
                    <div className="row align-item-center justify-content-center">
                        <div className="col-md-6 col-md-offset-3">
                            <div className="irs-section-title p-t-35">
                                <h2>CREATE DONATION</h2>
                                <div className="irs-title-line">
                                    <div
                                        className="irs-title-icon"
                                        style={{ left: "41%" }}>
                                        <img src="https://donatio.love/resources/assets/public/images/icon.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row create-donation-form bg-white">
                        <form
                            id="loginForm"
                            className="login100-form validate-form p-l-15 p-r-15"
                            onSubmit={handleSubmit}>
                            {/* <span className="login100-form-title">Sign In</span> */}
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="wrap-input100 validate-input m-b-16">
                                        <input
                                            type="text"
                                            autoFocus
                                            className="p-100"
                                            required
                                            className="input100"
                                            required
                                            name="title"
                                            placeholder="Title"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.title}
                                        />
                                        <span className="focus-input100" />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="m-b-16 ">
                                        <select
                                            className="input100 slect-field form-control"
                                            required
                                            name="category"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.category}
                                            required>
                                            <option defaultValue value="1">
                                                Category
                                            </option>
                                            <option value="food">Food</option>
                                            <option value="clothes">
                                                Clothes
                                            </option>
                                            <option value="shoes">Shoes</option>
                                            <option value="books">Books</option>
                                            <option value="blood">Blood</option>
                                            <option value="body-organs">
                                                Body Organs
                                            </option>
                                            <option value="bags">Bags</option>
                                            <option value="sun-glasses">
                                                Sun Glasses
                                            </option>
                                            <option value="household_stuff">
                                                Household Stuff
                                            </option>
                                            <option value="mobile_accessories">
                                                Mobile Accessories
                                            </option>
                                            <option value="computer_accessories">
                                                Computer Accessories
                                            </option>
                                            <option value="electronics">
                                                Electronics
                                            </option>
                                            <option value="furniture_items">
                                                Furniture/Wood Items
                                            </option>
                                            <option value="others">
                                                Others
                                            </option>
                                        </select>
                                        <span className="focus-input100" />
                                    </div>
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            className="input100"
                                            type="text"
                                            required
                                            name="country"
                                            placeholder="Country"
                                            onChange={this.handleCountryChange}
                                            onBlur={handleBlur}
                                            value={this.state.country}
                                            list="countries"
                                        />
                                        <span className="focus-input100" />
                                    </div>
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            className="input100"
                                            type="text"
                                            required
                                            name="state"
                                            placeholder="State"
                                            onChange={this.handleStateChange}
                                            onBlur={handleBlur}
                                            value={this.state.state}
                                            list="states"
                                        />
                                        <span className="focus-input100" />
                                    </div>
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            className="input100"
                                            type="text"
                                            required
                                            name="city"
                                            placeholder="City"
                                            onChange={this.handleCityChange}
                                            onBlur={handleBlur}
                                            value={this.state.city}
                                            list="cities"
                                        />
                                        <span className="focus-input100" />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="wrap-input100 validate-input m-b-16">
                                        <select
                                            className="input100 form-control"
                                            required
                                            name="product_condition"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.product_condition}>
                                            <option defaultValue value="n_a">
                                                Product Condition
                                            </option>
                                            <option value="new">New</option>
                                            <option value="Used Useable">
                                                Used & Useable
                                            </option>
                                            <option value="Normal Condition">
                                                Normal Condition
                                            </option>
                                            <option value="Poor Condition">
                                                Poor Condition
                                            </option>
                                        </select>
                                        <span className="focus-input100" />
                                    </div>
                                    <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            className="input100"
                                            type="number"
                                            required
                                            name="approx_value"
                                            placeholder="Approx. Value"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.approx_value}
                                        />
                                        <span className="focus-input100" />
                                    </div>
                                    <div className="wrap-input100 validate-input m-b-16">
                                        <input
                                            className="input100"
                                            type="number"
                                            required
                                            name="post_code"
                                            placeholder="Post Code"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.post_code}
                                        />
                                        <span className="focus-input100" />
                                    </div>
                                    <div className="wrap-input100  m-b-16 validate-input">
                                        <OverlayTrigger
                                            placement="right"
                                            overlay={
                                                <Tooltip id={`tooltip-right`}>
                                                    Separate each tag with a
                                                    comma , "i.e shoes, shirt"
                                                </Tooltip>
                                            }>
                                            <input
                                                className="input100"
                                                type="text"
                                                required
                                                name="tags"
                                                placeholder="Tags"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.tags}
                                            />
                                        </OverlayTrigger>
                                        <span className="focus-input100" />
                                    </div>
                                </div>
                                <div className="col-md-12">
                                <div className="wrap-input100 m-b-16 validate-input">
                                        <input
                                            required
                                            className="input100"
                                            type="text"
                                            name="area"
                                            placeholder="Address"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.area}
                                        />
                                        <span className="focus-input100" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12  m-b-16">
                                    <textarea
                                        className="input100 p-t-5 p-l-10 bg-light text-area"
                                        style={{ paddingTop: "15px" }}
                                        id="desc"
                                        cols="30"
                                        rows="5"
                                        required
                                        name="description"
                                        placeholder="Description"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.description}
                                    />
                                </div>
                            </div>
                            <p className="p-l-10 text-dark mb-2">
                                Upload Images{" "}
                                <span
                                    style={{
                                        color: "#ef004c",
                                        fontSize: "13px"
                                    }}>
                                    (Max 10 images can be attached)
                                </span>{" "}
                            </p>
                            <div className="m-b-16 uploadImage pl-2">
                                {/* <UploadImg /> */}
                                <button
                                    type="button"
                                    className="btn mr-3"
                                    style={{ border: "1px solid black" }}
                                    onClick={() =>
                                        document
                                            .querySelectorAll("[multiple]")[0]
                                            .click()
                                    }>
                                    Upload Images
                                    <i
                                        className="fas fa-cloud-upload-alt ml-2"
                                        style={{ color: "#ef004c" }}></i>{" "}
                                </button>
                                <div className="d-none">
                                    <FileBase64
                                        multiple={true}
                                        onDone={this.getFiles}
                                    />
                                </div>
                                <h6 style={{ display: "contents" }}>
                                    {this.state.images.length} Files Selected
                                </h6>
                                <div className="row">
                                    {this.state.error ? (
                                        <div
                                            className="alert alert-danger alert-dismissible fade show ml-3 mt-2"
                                            role="alert">
                                            {this.state.error}
                                            <button
                                                type="button"
                                                className="close"
                                                data-dismiss="alert"
                                                aria-label="Close">
                                                <span aria-hidden="true">
                                                    &times;
                                                </span>
                                            </button>
                                        </div>
                                    ) : null}
                                    {this.state.images.map((data, index) => (
                                        <div key={index} className="col-md-3">
                                            <img
                                                className="img-thumbnail"
                                                style={{
                                                    height: "110px",
                                                    width: "100%"
                                                }}
                                                src={data}
                                            />

                                            <i
                                                style={{
                                                    position: "absolute",
                                                    top: "5px",
                                                    left: "107px",
                                                    fontSize: "1.2em",
                                                    color: "red",
                                                    background: "white",
                                                    borderRadius: "50%"
                                                }}
                                                onClick={() =>
                                                    this.handleDelete(data)
                                                }
                                                className="fas fa-minus-circle"></i>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <div className="container-login100-form-btn p-t-20">
                                <button
                                    type="submit"
                                    className="login100-form-btn">
                                    {isSubmitting ? (
                                        <Spinner
                                            size={20}
                                            spinnerColor={"#fff"}
                                            spinnerWidth={2}
                                            visible={true}
                                        />
                                    ) : (
                                        "CREATE NOW"
                                    )}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                {/* <Footer /> */}
                <datalist id="countries">
                    {
                        countries.data && countries.data.map(country=> 
                            <option value={`${country.name} (+${country.phonecode})`} key={country.id} />
                            )
                    }
                </datalist>
                <datalist id="states">
                    {
                        states.data && states.data.map(state=> 
                            <option value={`${state.name}`} key={state.id} />
                            )
                    }
                </datalist>
                <datalist id="cities">
                    {
                        cities.data && cities.data.map(city=> 
                            <option value={`${city.name}`} key={city.id} />
                            )
                    }
                </datalist>
            </div>
        );
    }
}

const CD = withRouter(withFormik({
    mapPropsToValues: (props) => ({
        username: "",
        title: "",
        category: "",
        country: "",
        state: "",
        city: "",
        area: "",
        product_condition: "",
        approx_value: "",
        post_code: "",
        tags: "",
        description: "",
        images: [],
    }),
    handleSubmit: (values, { props, setSubmitting }) => {
        setTimeout(async () => {
            if (values.images.length == 0){
                setSubmitting(false);
                toast.info("Minimum 1 images must be selected!");
            }
            else {
                const data = {
                    ...values,
                    url: `${values.username}/${slugify(values.title).toLowerCase()+"-"+Math.round(Math.random() * 10000000000000)}`
                }
                console.log(data)
                await axios
                    .post("api/createpost", data)
                    .then((response) => {
                        if (response.data.success) {
                            toast.success("✔ Post Created Successfully!", {
                                autoClose: 2000
                            });
                            setTimeout(() => props.history.push("/my-donations"), 2000);
                        }
                        return response;
                    })
                    .catch((error) => {
                        toast.error("Ops! Something went wrong!");
                        console.log(error.response);
                    });  
                setSubmitting(false);
            }
        }, 1000);
    },

    displayName: "BasicForm"
})(CreateDonation))

function mapStateToProps(state) {
    return {
        user: state.user.credentials,
        UI: state.UI,
        countries: state.countries,
        states: state.states,
        cities: state.cities
    };
}

const mapDispatchToProps = {
    setCountries,
    setStates,
    setCities,
    setUserData
}

export default connect(mapStateToProps, mapDispatchToProps)(CD);
