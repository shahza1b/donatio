import {
    SET_LATEST_POSTS,
    SET_POST,
    LOADING_DATA,
    TOTAL_DONATIONS
} from "../consts";

const initialState = {
    loading: false,
    data: {
        donations: [],
        donated: []
    },
    post: {},
    moreFromThisUser: null,
    count: null
};

let more = []
let related = []
// let donations = []
// let donated = []


export default (state = initialState, action) => {
    switch (action.type) {
        case SET_LATEST_POSTS:
            // donations = action.payload.filter(post => post.donated == 0)
            // donated = action.payload.filter(post => post.donated == 1)
            return Object.assign({}, state, {
                loading: false,
                data: {
                    donations: action.payload.filter(post => post.donated == 0),
                    donated: action.payload.filter(post => post.donated == 1)
                }
            });
        case TOTAL_DONATIONS:
            return Object.assign({}, state, {
                count: action.payload
            });
        case SET_POST:
            more = action.payload.more.filter(post => post.id != action.payload.post.id)
            related = action.payload.related.filter(post => post.id != action.payload.post.id)
            return Object.assign({}, state, {
                loading: false,
                post: action.payload.post,
                moreFromThisUser: more.length > 0 ? more.reverse(): more,
                relatedPosts: related.length > 0 ? related.reverse() : related
            });
        case LOADING_DATA:
            return Object.assign({}, state, {
                loading: true
            });

        default:
            return state;
    }
};
