import { SHOW_MODAL } from "../consts";
import { SET_FORM } from "./../consts";

const initialState = {
    show: false,
    form: "LOGIN"
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_FORM:
            return Object.assign({}, state, {
                show: true,
                form: action.payload
            });
        case SHOW_MODAL:
            return Object.assign({}, state, {
                show: action.payload
            });

        default:
            return state;
    }
};
