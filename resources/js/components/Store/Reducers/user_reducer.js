import {
    USER_LOGIN,
    USER_LOGOUT,
    SET_USER_DATA
} from "../consts";

const initialState = { 
    isLoggedIn: false, 
    credentials: {}
};

export default function(state = initialState, action) {
    switch (action.type) {
        case USER_LOGIN:
            return Object.assign({}, state, {
                isLoggedIn: true,
                credentials: {
                    profileImage: action.payload.profileImage
                }
            });
        case USER_LOGOUT:
            return initialState;
        case SET_USER_DATA:
            return Object.assign({}, state, {
                credentials: action.payload
            })
        default:
            return state;
    }
}
