import { SET_CITIES } from "../consts";

const initialState = {
    data: null
}

export default ( state= initialState, action) => {
    switch(action.type) {
        case SET_CITIES: 
            return Object.assign({}, state, {
                data: action.payload
            })
        default: 
            return state
    }
}   