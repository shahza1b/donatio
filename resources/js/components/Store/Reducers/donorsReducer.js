import { 
    SET_DONORS, 
    // SET_TOTAL_DONORS, 
    SET_DONOR_PROFILE, 
    LOADING_DONOR_PROFILE
} from "../consts";

const initialState = {
    data: null,
    count: null,
    profile: null,
    loading: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_DONORS:
            return Object.assign({}, state, {
                data: action.donors,
                count: action.count
            });
        case SET_DONOR_PROFILE:
            return Object.assign({}, state, {
                profile: action.payload,
                loading: false
            })
        case LOADING_DONOR_PROFILE:
            return Object.assign({}, state, {
                loading: true
            })
        default:
            return state;
    }
};
