import { SET_RECEIVER_COUNT } from "../consts";

const initialState = {
    count: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_RECEIVER_COUNT:
            return Object.assign({}, state, {
                count: action.payload
            });
        default:
            return state;
    }
};
