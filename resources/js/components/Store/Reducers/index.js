import { combineReducers } from "redux";
import uiReducer from "./uiReducer.js";
import countryReducer from "./countryReducer.js";
import stateReducer from "./stateReducer.js";
import cityReducer from "./cityReducer.js";
import donorsReducer from "./donorsReducer.js";
import postsReducer from "./postsReducer.js";
import userReducer from "./user_reducer.js";
import modalReducer from "./modalReducer.js";
import recieverReducer from "./recieverReducer.js";



const rootReducer = combineReducers({
	user: userReducer,
	UI: uiReducer,
	Modal: modalReducer,
	countries: countryReducer,
	states: stateReducer,
	cities: cityReducer,
	donors: donorsReducer,
	reciever: recieverReducer,
	latestPosts: postsReducer
});

export default rootReducer;
