// Redux
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./Reducers";

// Redux Persist
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";

// Integrate Redux develop tools
import { composeWithDevTools } from "redux-devtools-extension";


const persistConfig = {
    key: "donatio",
    storage: storage,
    stateReconciler: autoMergeLevel2,
    whitelist: ['user'],
    // blacklist: ["countries", "states", "cities", "UI", "latestPosts", "donors"]
    // transforms: [
    //     UISubsetBlacklistFilter, //need import
    // ]
};

const pReducer = persistReducer(persistConfig, rootReducer);

export const Store = createStore(
    pReducer,
    {},
    composeWithDevTools(applyMiddleware(thunk))
);
export const persistor = persistStore(Store);


