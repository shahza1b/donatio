// USER
export const USER_LOGIN = "user_login";
export const USER_LOGOUT = "user_logout";
export const SET_USER_DATA = "set_user_data";

// POST
export const CREATE_POST = "create_post";
export const SET_POST = "set_post";
export const SET_LATEST_POSTS = "set_latest_posts";
export const LOADING_DATA = "loading_data";
export const TOTAL_DONATIONS = "total_donations";

// GENERAL
export const SET_COUNTRY = "set_country";
export const SET_STATES = "set_states";
export const SET_CITIES = "set_cities";

// UI
export const SET_LOADING = "set_loading";
export const UNSET_LOADING = "unset_loading";
export const SET_ERRORS = "set_errors";
export const CLEAR_ERRORS = "clear_errors";
export const SET_FORM = "set_form";
export const SHOW_MODAL = "show_modal";

// DONORS
export const SET_DONORS = "set_donors";
export const SET_TOTAL_DONORS = "set_total_donors";
export const SET_DONOR_PROFILE = "set_donor_profile";
export const LOADING_DONOR_PROFILE = "loading_donor_profile";

// RECIEVER
export const SET_RECEIVER_COUNT = "set_receiver_count";


