// import axios from "axios";
import {
    SET_LOADING,
    SET_DONORS,
    UNSET_LOADING,
    SET_RECEIVER_COUNT,
    SET_DONOR_PROFILE,
    // SET_TOTAL_DONORS
} from "../consts";

export const setDonors = () => dispatch => {
    axios
        .all([
            axios.get("/api/donors"), 
            axios.get("/api/donors/count"),
            axios.get('/api/receiver/count')
        ])
        .then(async res => {
            // console.log(res)
            dispatch({ type: SET_LOADING });
            // Set donors
            await dispatch({ 
                type: SET_DONORS, 
                donors: res[0].data.donors,
                count: res[1].data
            });
            dispatch({
                type: SET_RECEIVER_COUNT,
                payload: res[2].data
            })
            // setReceiverCount(res[2].data)
            dispatch({ type: UNSET_LOADING });
        })
        .catch(err => console.log(err));
};

// Get Donor Profile
export const setDonorProfile = (username) => dispatch => {
    axios.get(`/api/donor/${username}`)
        .then(res => {

            dispatch({
                type: SET_DONOR_PROFILE,
                payload: res.data
            })
            dispatch({
                type: UNSET_LOADING
            })
        })
        .catch(err => {
            console.log(err)
            dispatch({
                type: SET_DONOR_PROFILE,
                payload: err.response.data
            })
        })
}



export const setReceiverCount = (count) => dispatch => {
    console.log(count)
    dispatch({
        type: SET_RECEIVER_COUNT,
        payload: count
    })
}