import {
    USER_LOGIN,
    USER_LOGOUT,
    SET_ERRORS,
    CLEAR_ERRORS,
    SET_USER_DATA,
    UNSET_LOADING
} from "../consts";
import axios from "axios";
import { toast } from "react-toastify";


// REGISTER USER ACTION
export const userRegister = (newUser, history) => dispatch => {
    axios
        .post("/api/user/register", newUser)
        .then(res => {
            toast.success(`✔ ${res.data.message}`, {
                autoClose: 2500
            });
            dispatch({ type: UNSET_LOADING });
            setTimeout(() => (history.push("/#")), 3000);
        })
        .catch(error => {
            console.log(error.response);
            dispatch({ type: SET_ERRORS, payload: error.response.data });
        });
};

// LOGIN USER ACTION
export const userLogin = (user, history) => dispatch => {
    axios
        .post("/api/user/login", user)
        .then(async res => {
            dispatch({ type: CLEAR_ERRORS });
            // save token in local storage
            localStorage["DonatioIdToken"] = `Bearer ${res.data.token}`;
            axios.defaults.headers.common["Authorization"] = `Bearer ${res.data.token}`
            document.cookie = `token=${res.data.token}`
            toast.success(
                "✔ Login Successful. You'll be redirected to dashboard shortly...",
                {
                    autoClose: 1500
                }
            );
            await dispatch({ type: USER_LOGIN, payload: res.data });
            await dispatch({ type: UNSET_LOADING });
            // setTimeout(() => (location.href = "/dashboard"), 2000);
            setTimeout(() => (history.push("/dashboard")), 1000);
        })
        .catch(error => {
            console.log(error.response)
            dispatch({ type: SET_ERRORS, payload: error.response.data });
        });
};

// LOGOUT USER
export const userLogout = () => dispatch => {
    localStorage.clear();
    document.cookie = 'token=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    dispatch({ type: CLEAR_ERRORS });
    delete axios.defaults.headers.common["Authorization"]
    setTimeout(() => (dispatch({ type: USER_LOGOUT })), 1000)
};



// Set Authenticated User data
export const setUserData = (print) => dispatch => {
    axios
        .get("/api/user")
        .then(res => {
            dispatch({ type: SET_USER_DATA, payload: res.data[0] });
        })
        .catch(error => console.log(error.response));
};



// UPDATE USER PROFILE DATA
export const updateProfile = data => dispatch => {
    axios
        .post("/api/user", data)
        .then(async () => {
            await dispatch(setUserData("Update profile"))
            await setUserData()
            toast.success("✔ Profile Updated Successfully", {
                autoClose: 1500
            });
        })
        .catch(err => {
            toast.error(err.response.data);
            console.log(err.response);
        });
};



// Reset Password
export const changePassword = data => dispatch => {
    axios
        .post("/api/user/resetPassword", data)
        .then(async res => {
            await dispatch({ type: SET_ERRORS, payload: res.data });
            dispatch({ type: UNSET_LOADING });
            // setTimeout(() => (location.reload()), 1500)
        })
        .catch(async err => {
            console.log(err.response);
            await dispatch({ type: SET_ERRORS, payload: err.response.data });
        });
};
