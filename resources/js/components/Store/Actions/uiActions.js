import { SET_FORM } from './../consts';

export const setForm = form => dispatch => {
    dispatch({
        type: SET_FORM,
        payload: form
    })
}