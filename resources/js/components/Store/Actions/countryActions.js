import axios from 'axios'
import { SET_COUNTRY, SET_STATES, SET_CITIES } from '../consts'


export const setCountries = (search) => dispatch => {
    axios.get(`/api/countriesList/${search}`)
    .then( res => {
         dispatch({type: SET_COUNTRY, payload: res.data.countriesList})
    })
    .catch(err => console.log(err.response))
}


export const setStates = (search) => dispatch => {
    axios.get(`/api/states/${search}`)
    .then(res => {
        dispatch({type: SET_STATES, payload: res.data.states})
    })
    .catch(err => console.log(err.response))
}


export const setCities = (search) => dispatch => {
    axios.get(`/api/cities/${search}`)
    .then(res => {
        dispatch({type: SET_CITIES, payload: res.data.cities})
    })
    .catch(err => console.log(err.response))
}
