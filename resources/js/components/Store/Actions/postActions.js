import {
    SET_LATEST_POSTS,
    LOADING_DATA,
    SET_POST,
    TOTAL_DONATIONS
} from "../consts";

// get all donations/latest donations
export const setLatestPosts = () => dispatch => {
    dispatch({ type: LOADING_DATA });
    axios 
        .get("/api/getlatestposts")
        .then(response => {
            dispatch({
                type: SET_LATEST_POSTS,
                payload: response.data
            });
            dispatch(totalDonations());
        })
        .catch(error => {
            console.log(error);
        });
};


// Donation count
export const totalDonations = () => dispatch => {
    axios
        .get("/api/posts/count")
        .then(res => {
            dispatch({
                type: TOTAL_DONATIONS,
                payload: res.data
            });
        })
        .catch(error => console.log(error));
};

// Get single donation
export const setPost = (username, title) => dispatch => {
    dispatch({ type: LOADING_DATA });
    axios
        .get(`/api/post/${username}/${title}`)
        .then(res => {
            dispatch({
                type: SET_POST,
                payload: res.data
            });
        })
        .catch(error => {
            console.log(error)
            dispatch({
                type: SET_POST,
                payload: error.response.data
            });
        });
};
