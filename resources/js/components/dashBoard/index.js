import React, { useEffect } from "react";
// import Header from "./../reusableComponents/Header";
// import Footer from "./../reusableComponents/Footer";
import Details from "./details";
import DashboardGraph from "./dashboardGraph";
import { useDispatch } from 'react-redux'
import { setUserData } from "../Store/Actions";

const Dashboard = () =>{
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch( setUserData("Dashboard") ) 
    }, [])
        return (
            <div>
                {/* <Header /> */}
                <Details />
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <DashboardGraph />
                        </div>
                    </div>
                </div>
                {/* <Footer /> */}
            </div>
        );
}

export default Dashboard;
