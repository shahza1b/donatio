import React, { Component } from "react";


class Details extends Component{
    render(){
        return (
            <div className="container">
                <div className="dashboard mt-5">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="dashboard_content row">
                            <div className="col-md-4 col-sm-12">
                                <div
                                    className="dash_content"
                                    style={{ border: "1px solid  #fc8861" }}
                                >
                                    <div
                                        className="dash_img"
                                        style={{ background: "#fc8861" }}
                                    >
                                        <img src="https://donatio.love/resources/assets/public/images/dashboard/active-donations.png" />
                                    </div>
                                    <div className="dash_donations">
                                        <h5 className="orange">ACTIVE DONATIONS</h5>
                                        <h3>0</h3>
                                    </div>
                                    <div className="clearfix" />
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <div
                                    className="dash_content"
                                    style={{ border: "1px solid  #56bdde" }}
                                >
                                    <div
                                        className="dash_img"
                                        style={{ background: "#56bdde" }}
                                    >
                                        <img src="https://donatio.love/resources/assets/public/images/dashboard/given-donations.png" />
                                    </div>
                                    <div className="dash_donations">
                                        <h5 className="aqua">DONATED</h5>
                                        <h3>1</h3>
                                    </div>
                                    <div className="clearfix" />
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <div
                                    className="dash_content"
                                    style={{ border: "1px solid #b399e1" }}
                                >
                                    <div
                                        className="dash_img"
                                        style={{ background: "#b399e1" }}
                                    >
                                        <img src="https://donatio.love/resources/assets/public/images/dashboard/total-donations.png" />
                                    </div>
                                    <div className="dash_donations">
                                        <h5 className="purple">MY DONATIONS</h5>
                                        <h3>1</h3>
                                    </div>
                                    <div className="clearfix" />
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <div
                                    className="dash_content"
                                    style={{ border: "1px solid #fc6161" }}
                                >
                                    <div
                                        className="dash_img"
                                        style={{ background: "#fc6161" }}
                                    >
                                        <img src="https://donatio.love/resources/assets/public/images/dashboard/request-received.png" />
                                    </div>
                                    <div className="dash_donations">
                                        <h5 className="redd">REQUESTS RECEIVED</h5>
                                        <h3>1</h3>
                                    </div>
                                    <div className="clearfix" />
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <div
                                    className="dash_content"
                                    style={{ border: "1px solid #3ddc97" }}
                                >
                                    <div
                                        className="dash_img"
                                        style={{ background: "#3ddc97" }}
                                    >
                                        <img src="https://donatio.love/resources/assets/public/images/dashboard/sent_req.png" />
                                    </div>
                                    <div className="dash_donations">
                                        <h5 className="green">MY REQUESTS</h5>
                                        <h3>0</h3>
                                    </div>
                                    <div className="clearfix" />
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <div
                                    className="dash_content"
                                    style={{ border: "1px solid #ffc300" }}
                                >
                                    <div
                                        className="dash_img"
                                        style={{ background: "#ffc300" }}
                                    >
                                        <img src="https://donatio.love/resources/assets/public/images/dashboard/review-icon.png" />
                                    </div>
                                    <div className="dash_donations">
                                        <h5 className="YELLOW">REVIEWS</h5>
                                        <h3>1</h3>
                                    </div>
                                    <div className="clearfix" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default Details;
