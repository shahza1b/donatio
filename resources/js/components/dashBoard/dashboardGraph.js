import React, { PureComponent } from "react";
import {
    BarChart,
    Bar,
    Cell,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend
} from "recharts";

const data = [
    {
        name: "ACTIVE DONATIONS",
        Total: 0
    },
    {
        name: "DONATED",
        Total: 1
    },
    {
        name: "MY DONATIONS",
        Total: 50
    },
    {
        name: "REQUESTS RECIEVED",
        Total: 1
    },
    {
        name: "MY REQUESTS",
        Total: 30
    },
    {
        name: "REVIEWS",
        Total: 1
    }
];
const COLORS = [
    "#fc8861",
    "#56bdde",
    "#b399e1",
    "#fc6161",
    "#3ddc97",
    "#ffc300"
];
export default class DashboardGraph extends PureComponent {
    render() {
        return (
            <BarChart
                width={1100}
                height={500}
                data={data}
                margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5
                }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Bar dataKey="Total">
                    {data.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={COLORS[index]} />
                    ))}
                </Bar>
            </BarChart>
        );
    }
}
