import React, { Component } from "react";
import Header from "../reusableComponents/Header";
import Footer from "../reusableComponents/Footer";
import GoogleApiWrapper from "./../reusableComponents/GoogleMap";
import { withFormik } from "formik";

const ContactUs = props => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit
    } = props;
    return (
        <div>
            {/* <Header /> */}
            <section className="irs-contact-field">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="irs-contact-col m-0">
                                <h4>Get In Touch</h4>
                                <form onSubmit={handleSubmit}>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="wrap-input100 validate-input m-b-16">
                                                <input
                                                    required
                                                    type="text"
                                                    id="form_name"
                                                    name="name"
                                                    className="p-100 input100"
                                                    placeholder="Name*"
                                                    data-error="Name is required."
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.name}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="wrap-input100 validate-input m-b-16">
                                                <input
                                                    required
                                                    type="email"
                                                    className="p-100 input100"
                                                    name="email"
                                                    data-erroe="Email is required"
                                                    placeholder="Email"
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.email}
                                                />
                                                <span className="focus-input100" />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="wrap-input100 validate-input m-b-16">
                                                <input
                                                    required
                                                    type="text"
                                                    id="form_sub"
                                                    name="subject"
                                                    className="p-100 input100"
                                                    placeholder="Subject*"
                                                    data-error="Name is subject."
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.subject}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="wrap-input100 validate-input m-b-16">
                                                <textarea
                                                    id="form_message"
                                                    name="message"
                                                    className="p-100 input1000"
                                                    rows="6"
                                                    placeholder="Type Your Message......."
                                                    data-error="Message is required."
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.message}
                                                    style={{
                                                        marginBottom:
                                                            "0px !important"
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="text-center m-b-10 p-t-20">
                                                <button
                                                    type="submit"
                                                    className="btn btn-danger p-t-10"
                                                    style={{
                                                        width: "25%",
                                                        padding: "14px 18px",
                                                        borderRadius: "25px"
                                                    }}
                                                >
                                                    SEND
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <GoogleApiWrapper />
                        </div>
                    </div>
                </div>
            </section>

            {/* <Footer /> */}
        </div>
    );
};
export default withFormik({
    mapPropsToValues: () => ({
        name: "",
        email: "",
        subject: "",
        message: ""
    }),

    // Custom sync validation
    // validate: (values) => {
    //     const errors = {};

    //     if (!values.email) {
    //         errors.email = "Required";
    //     }
    //     if (!values.password) {
    //         errors.password = "Required";
    //     }

    //     return errors;
    // },

    handleSubmit: (values, { setSubmitting }) => {
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: "BasicForm"
})(ContactUs);
