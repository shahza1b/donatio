import React, { useEffect, useState } from "react";
// import { Link } from "react-router-dom";
import DonorProfileCard from "./../reusableComponents/DonorProfileCard";
import { setDonors } from "../Store/Actions/donorsActions";
import { useSelector, useDispatch } from "react-redux";

const AllDonors = () => {
    const dispatch = useDispatch();
    const donors = useSelector(state => state.donors);

    useEffect(() => {
        dispatch(setDonors());
    }, []);

    return (
        <div>
            {/* <Header /> */}
            <section
                className="irs-volunteers-field"
                // style={{ height: "100vh" }}
            >
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 col-md-offset-0">
                            <div className="irs-section-title">
                                <h2>Great people who are donating with love</h2>
                                <div className="irs-title-line">
                                    <div
                                        className="irs-title-icon "
                                        style={{ left: "41%" }}
                                    >
                                        <img
                                            src="https://donatio.love/resources/assets/public/images/icon.png"
                                            alt="icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row row-centered">
                        {donors.data &&
                            donors.data.map((donor, i) => (
                                <DonorProfileCard
                                    key={i}
                                    name={donor.name}
                                    location={donor.location}
                                    rating={donor.rating}
                                    profileImage={donor.profileImage}
                                    username={donor.username}
                                />
                            ))}
                    </div>
                    {/* <div className="text-center p-t-40">
                        <Link
                            className="btn btn-outline-danger"
                            to="/donors"
                            role="button"
                            style={{
                                padding: "12px 18px",
                                borderRadius: "25px"
                            }}
                            onClick={this.scrollToTop}>
                            VIEW ALL
                        </Link>
                    </div> */}
                </div>
            </section>

            {/* <Footer /> */}
        </div>
    );
};

export default AllDonors;
