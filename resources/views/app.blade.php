<!DOCTYPE html>
    <html lang="{{ app()->getLocale() }}">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- TItle -->
        <title>Donatio.love</title>
        <!-- Styles -->
        <link rel="icon" href="{{asset('favicon.png')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}?v=1.1">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}?v=1.1">
        <link rel="stylesheet" href="{{asset('css/loginStyles.css')}}?v=1.1">
        <link rel="stylesheet" href="{{asset('css/main.css')}}?v=1.1">
        <link rel="stylesheet" href="{{asset('css/util.css')}}?v=1.1">
        <!--===============================================================================================-->
        <link rel="stylesheet" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        
    </head>
    <body>
        <div id="app"></div>
        
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
    </html>
