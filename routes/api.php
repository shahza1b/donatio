<?php

Route::group(['middleware' => 'api-header'], function () {

    // The registration and login requests doesn't come with tokens
    // as users at that point have not been authenticated yet
    // Therefore the jwtMiddleware will be exclusive of them
    Route::post('user/login', 'UserController@login');
    Route::post('user/register', 'UserController@register');

    Route::get('getlatestposts', 'PostController@showLatestPosts');
    Route::get('getuserrating', 'UserController@getUserRating');
    Route::get('/post/{username}/{title}', 'PostController@show');
    Route::get('/posts/count', 'PostController@count');

// Country List API
    Route::get('/countriesList/{search}', 'CountriesController@get');
    Route::get('/states/{search}', 'CountriesController@states');
    Route::get('/cities/{search}', 'CountriesController@cities');

// Donors
    Route::get('/donors', 'DonorController@get');
    Route::get('/donors/count', 'DonorController@count');
    Route::get('/donor/{username}', 'DonorController@show');

// Reciever
    Route::get('/receiver/count', 'RecieverController@count');
});

Route::group(['middleware' => ['jwt.auth', 'api-header']], function () {

    // // all routes to protected resources are registered here
    // Route::get('test/middleware', function () {
    //     return response()->json("working");
    // });

    Route::post('getUserPosts', 'PostController@showAllUserPosts');
    Route::post('/user', 'UserController@updateProfile');
    Route::post('/user/resetPassword', 'UserController@changePassword');
    Route::get('/user', 'UserController@getAuthenticatedUser');
// Route::post('forgot/password', 'Auth\ForgotPasswordController')->name('password.reset');

// Route::post('forgot/password', 'Api\ForgotPasswordController')->name('forgot.password');

    Route::post('createpost', 'PostController@create');
});






// CLEAR CACHE
Route::get('/laravel/clear-cache', function () {

    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');

    return "Cleared!";

});

