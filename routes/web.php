<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::view('/{path?}', 'app');
Route::get( '/{path?}', function(){
    return view( 'app' );
} )->where('path', '.*');

// Route::group(['middleware' => 'api-header'], function () {

//     // The registration and login requests doesn't come with tokens
//     // as users at that point have not been authenticated yet
//     // Therefore the jwtMiddleware will be exclusive of them
//     Route::post('web/login', 'UserController@login');
//     Route::post('web/register', 'UserController@register');
// });

